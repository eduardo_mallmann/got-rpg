package com.eduardomallmann.auto1.gotterminalrpg;

import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;
import com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils;
import com.eduardomallmann.auto1.gotterminalrpg.game.GameController;
import com.eduardomallmann.auto1.gotterminalrpg.users.LoginController;
import com.eduardomallmann.auto1.gotterminalrpg.users.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Main class responsible to run the application.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class Main {

	/**
	 * Main method called when application starts.
	 *
	 * @param args default args params.
	 */
	public static void main(final String[] args) {

		LoginController loginController = new LoginController();

		List<User> users = new ArrayList<>();

		try {

			users.addAll(loginController.retrieveAllUsers());

		} catch (GameException e) {
			System.out.println(ResourceUtils.getScriptAlert(e.getErrorKey()));
			System.exit(-1);
		}

		User user = loginController.loginViewController(users);

		GameController.start(users, user);
	}
}
