/**
 * Provides the helper classes to the application.
 *
 * @since 1.0
 */
package com.eduardomallmann.auto1.gotterminalrpg.commons.utils;