package com.eduardomallmann.auto1.gotterminalrpg.commons.enums;

/**
 * Enum that holds the colors of the application.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public enum TerminalColor {

    BLACK("30m"),
    RED("31m"),
    GREEN("32m"),
    YELLOW("33m"),
    BLUE("34m"),
    PURPLE("35m"),
    CYAN("36m"),
    WHITE("37m"),
    BRIGHT_BLACK("90m"),
    BRIGHT_RED("91m"),
    BRIGHT_GREEN("92m"),
    BRIGHT_YELLOW("93m"),
    BRIGHT_BLUE("94m"),
    BRIGHT_PURPLE("95m"),
    BRIGHT_CYAN("96m"),
    BRIGHT_WHITE("97m"),
    BK_BLACK("40m"),
    BK_RED("41m"),
    BK_GREEN("42m"),
    BK_YELLOW("43m"),
    BK_BLUE("44m"),
    BK_PURPLE("45m"),
    BK_CYAN("46m"),
    BK_WHITE("47m"),
    BK_BRIGHT_BLACK("100m"),
    BK_BRIGHT_RED("101m"),
    BK_BRIGHT_GREEN("102m"),
    BK_BRIGHT_YELLOW("103m"),
    BK_BRIGHT_BLUE("104m"),
    BK_BRIGHT_PURPLE("105m"),
    BK_BRIGHT_CYAN("106m"),
    BK_BRIGHT_WHITE("107m"),
    NO_COLOR("");

    private static final String BK_BRIGHT_REGEX = "^BK_BRIGHT_\\w*";
    private static final String BK_PREFIX = "BK_";
    private static final String FONT_COLOR_MATCH = "^(3)\\w+|^(9)\\w+";
    private String color;

    /**
     * Main constructor.
     *
     * @param color color param.
     */
    TerminalColor(final String color) {
        this.color = color;
    }

    /**
     * Builds the font color with the needed characters.
     *
     * @return the font color scape character.
     */
    public String build() {
        return (char) 27 + "[" + color;
    }

    /**
     * Builds the font color in bold with the needed characters. First it checks if the color informed is a font color.
     *
     * @return the bold font color scape character.
     */
    public String bold() {
        if (isFontColor(color)) {
            return (char) 27 + "[1;" + color;
        } else {
            return (char) 27 + "[" + color;
        }
    }

    /**
     * Builds the font color with underline with the needed characters. First it checks if the color informed is a font
     * color.
     *
     * @return the font color with underline scape character.
     */
    public String underline() {
        if (isFontColor(color)) {
            return (char) 27 + "[4;" + color;
        } else {
            return (char) 27 + "[" + color;
        }
    }

    /**
     * Checks if the color is a font color.
     *
     * @param color color to be checked.
     * @return boolean.
     */
    private boolean isFontColor(final String color) {
        return color.matches(FONT_COLOR_MATCH);
    }

    /**
     * @return the bold character.
     */
    public static String getBold() {
        return (char) 27 + "[1m";
    }

    /**
     * @return the underline character.
     */
    public static String getUnderline() {
        return (char) 27 + "[4m";
    }

    /**
     * @return reset color character.
     */
    public static String RESET() {
        return (char) 27 + "[0m";
    }

    /**
     * Retrives the TerminalColor object for the color name informed. If not found retrivies empty string.
     *
     * @param color color name.
     * @return TerminalColor found.
     */
    public static TerminalColor getColor(final String color) {

        for (TerminalColor value : TerminalColor.values()) {
            if (value.name().equalsIgnoreCase(color)) {
                return value;
            }
        }

        return NO_COLOR;
    }

    /**
     * Retrives the bright TerminalColor object for the color name informed. If not found retrivies empty string.
     *
     * @param color color name.
     * @return TerminalColor found.
     */
    public static TerminalColor getBrightColor(final String color) {

        for (TerminalColor value : TerminalColor.values()) {
            if (value.name().contains("_") && value.name().contains(color.toUpperCase())) {
                return value;
            }
        }

        return NO_COLOR;
    }

    /**
     * Retrives the background TerminalColor object for the color name informed. If not found retrivies empty string.
     *
     * @param color color name.
     * @return TerminalColor found.
     */
    public static TerminalColor getBKColor(final String color) {

        for (TerminalColor value : TerminalColor.values()) {
            if (value.name().startsWith(BK_PREFIX) && value.name().contains(color.toUpperCase())) {
                return value;
            }
        }

        return NO_COLOR;
    }

    /**
     * Retrives the bright background TerminalColor object for the color name informed. If not found retrivies empty
     * string.
     *
     * @param color color name.
     * @return TerminalColor found.
     */
    public static TerminalColor getBKBrightColor(final String color) {

        for (TerminalColor value : TerminalColor.values()) {
            if (value.name().matches(BK_BRIGHT_REGEX) && value.name().contains(color.toUpperCase())) {
                return value;
            }
        }

        return NO_COLOR;
    }

}
