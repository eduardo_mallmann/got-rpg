package com.eduardomallmann.auto1.gotterminalrpg.commons.utils;

import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.TerminalColor;

import java.util.ResourceBundle;

/**
 * Helper class responsible for handle resource bundle retrieve values.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class ResourceUtils {

    private static final ResourceBundle SCRIPT_RESOURCE = ResourceBundle.getBundle("game-script");
    private static final ResourceBundle CONFIG_RESOURCE = ResourceBundle.getBundle("config");

    private String resource;

    /**
     * Main constructor.
     *
     * @param resource resource name to be used.
     */
    public ResourceUtils(String resource) {
        this.resource = resource;
    }

    /**
     * Retrieve the value for the key informed from the game-script resource bundle.
     *
     * @param key key for the text to be retrieved.
     * @return The value of the key.
     */
    public static String getScript(String key) {
        return SCRIPT_RESOURCE.getString(key);
    }

    /**
     * Retrieve the value for the key informed from the game-script resource bundle in bold format.
     *
     * @param key key for the text to be retrieved.
     * @return The value of the key in bold format.
     */
    public static String getBoldScriptText(String key) {
        return TerminalColor.BRIGHT_GREEN.bold().concat(SCRIPT_RESOURCE.getString(key)).concat(TerminalColor.RESET());
    }

    /**
     * Retrieve the value for the key informed from the game-script resource bundle in alert format.
     *
     * @param key key for the text to be retrieved.
     * @return The value of the key in alert format.
     */
    public static String getScriptAlert(String key) {
        return TerminalColor.BK_BRIGHT_YELLOW.build().concat(TerminalColor.RED.bold())
                .concat(SCRIPT_RESOURCE.getString(key)).concat(TerminalColor.RESET());
    }

    /**
     * Retrieve the value for the key informed from the config resource bundle.
     *
     * @param key key for the text to be retrieved.
     * @return The value of the key.
     */
    public static String getConfig(String key) {
        return CONFIG_RESOURCE.getString(key);
    }

    /**
     * Retrieve the object value for the key informed from the resource bundle informed in the instantiation.
     *
     * @param key key for the text to be retrieved.
     * @return An object with the value of the key.
     */
    public Object getObjectValue(String key) {
        return ResourceBundle.getBundle(resource).getObject(key);
    }

    /**
     * Retrieve the string value for the key informed from the resource bundle informed in the instantiation.
     *
     * @param key key for the text to be retrieved.
     * @return The value of the key.
     */
    public String getStringValue(String key) {
        return ResourceBundle.getBundle(resource).getString(key);
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }
}
