/**
 * Provides the necessary objects to handle exceptions.
 *
 * @since 1.0
 */
package com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions;