package com.eduardomallmann.auto1.gotterminalrpg.commons;

import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;

import java.util.List;

public interface BaseService<T extends BaseModel> {

	/**
	 * Retrieve the list of T of the game.
	 *
	 * @return a list of T.
	 * @throws GameException when the list can't be retrieved.
	 */
	List<T> retrieveAll() throws GameException;

	/**
	 * Unwraps the T from the csv line.
	 *
	 * @param csvLine one line from csv file.
	 * @return T instance from the csv line.
	 */
	T unwrapCSV(final String csvLine);

	/**
	 * Wraps the T in a String with csv format.
	 *
	 * @return String in CSV format.
	 */
	String wrapCSV(final T object);
}
