package com.eduardomallmann.auto1.gotterminalrpg.commons.utils;

import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;

import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.getConfig;

/**
 * Helper class responsible for handle files operations to save users configurations.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class FileUtils {

    public static final String SYSTEM_FOLDER_PATH = System.getProperty("user.home").concat(File.separator)
            .concat(getConfig("system.files.folder"));

    /**
     * Save the file in the GOT config folder from computer.
     *
     * @param name        name of the file to be saved.
     * @param contentList content of the file to be saved.
     * @throws GameException when the file can't be saved.
     */
    public static void saveFile(final String name, final List<String> contentList) throws GameException {

        Path appFolder = createAppFolder();

        String filePathName = appFolder.toString().concat(File.separator).concat(name);

        String content = String.join("\n", contentList);

        writeFile(filePathName, content);
    }

    /**
     * Creates the folder to place the GOT users configurations.
     *
     * @return the path to the folder.
     * @throws GameException when the folder can't be created.
     */
    public static Path createAppFolder() throws GameException {

        Path folderPath = Paths.get(SYSTEM_FOLDER_PATH);

        if (Files.exists(folderPath)) {
            return folderPath;
        }

        try {

            return Files.createDirectory(folderPath);

        } catch (IOException e) {
            throw new GameException("error.files.folder");
        }
    }

    /**
     * Safely writes the file, rolling back for the previous one if it exists.
     *
     * @param filePathName complete path to file.
     * @param content      content to be write.
     * @throws GameException when the file can't be saved.
     */
    private static void writeFile(final String filePathName, final String content) throws GameException {

        Path filePath = Paths.get(filePathName);
        Path bkpFile = Paths.get(filePathName.concat(getConfig("system.file.bkp.ext")));

        try {

            checkAndBKP(filePath, bkpFile);

            Files.write(filePath, content.getBytes(),
                    StandardOpenOption.CREATE, LinkOption.NOFOLLOW_LINKS);

        } catch (Exception e) {

            throw new GameException("error.files.save");

        } finally {
            try {

                if (Files.exists(bkpFile)) Files.delete(bkpFile);

            } catch (IOException io) {

                //do nothing the next time the bkp will be replaced.
            }
        }
    }

    /**
     * Checks if the file exists and if positive, makes its backup.
     *
     * @param source file to get backup.
     * @param target backup file name.
     * @throws GameException when the backup fails.
     */
    private static void checkAndBKP(final Path source, final Path target) throws GameException {

        try {

            if (Files.exists(source)) {
                Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
            }

        } catch (IOException io) {

            throw new GameException("error.files.bkp");
        }

    }

}
