/**
 * Provides the enums for the application.
 *
 * @since 1.0
 */
package com.eduardomallmann.auto1.gotterminalrpg.commons.enums;