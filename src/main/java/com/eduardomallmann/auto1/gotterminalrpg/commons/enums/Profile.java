package com.eduardomallmann.auto1.gotterminalrpg.commons.enums;

/**
 * Enum that holds the character's profile of the game.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public enum Profile {

	PLAYER("player"),
	ALLY("ally"),
	ENEMY("enemy"),
	DEFEATED("defeated"),
	INITIAL("initial");

	private String profile;

	/**
	 * Main constructor.
	 *
	 * @param profile profile value
	 */
	Profile(final String profile) {
		this.profile = profile;
	}

	/**
	 * @return return the value of the enum.
	 */
	public String build() {
		return profile;
	}

	/**
	 * Retrieves the Profile for the value informed.
	 *
	 * @param profile value to be found.
	 * @return the Profile found.
	 */
	public static Profile getProfile(final String profile) {
		for (Profile value : Profile.values()) {
			if (value.name().equalsIgnoreCase(profile) || value.profile.equals(profile)) {
				return value;
			}
		}

		return Profile.INITIAL;
	}
}
