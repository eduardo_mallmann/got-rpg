package com.eduardomallmann.auto1.gotterminalrpg.commons.enums;

/**
 * Enum that holds the symbols characters of the game screen map.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public enum Symbol {

    PLAYER("\u001B[47m\u001B[90mi\u001B[0m"),
    ALLY("\u001B[34mi\u001B[0m"),
    ENEMY("\u001B[31mi\u001B[0m"),
    DEFEATED("\u001B[31mx\u001B[0m"),
    SEA("\u2592"),
    BORDER("#"),
    LAND("\u2593");

    private String symbol;

    /**
     * Main constructor.
     *
     * @param symbol value of the enum.
     */
    Symbol(final String symbol) {
        this.symbol = symbol;
    }

    /**
     * @return return the value of the enum.
     */
    public String build() {
        return symbol;
    }

    /**
     * Checks if the param informed is a player.
     *
     * @param type param to be checked.
     * @return boolean.
     */
    public static boolean isPlayer(final String type) {
        return PLAYER.symbol.equals(type) || PLAYER.name().equalsIgnoreCase(type);
    }

    /**
     * Checks if the param informed is an ally.
     *
     * @param type param to be checked.
     * @return boolean.
     */
    public static boolean isAlly(final String type) {
        return ALLY.symbol.equals(type) || ALLY.name().equalsIgnoreCase(type);
    }

    /**
     * Checks if the param informed is an enemy.
     *
     * @param type param to be checked.
     * @return boolean.
     */
    public static boolean isEnemy(final String type) {
        return ENEMY.symbol.equals(type) || ENEMY.name().equalsIgnoreCase(type);
    }

    /**
     * Checks if the param informed is defeated.
     *
     * @param type param to be checked.
     * @return boolean.
     */
    public static boolean isDefeated(final String type) {
        return DEFEATED.symbol.equals(type) || DEFEATED.name().equalsIgnoreCase(type);
    }

    /**
     * Checks if the param informed is the sea.
     *
     * @param type param to be checked.
     * @return boolean.
     */
    public static boolean isSea(final String type) {
        return SEA.symbol.equals(type) || SEA.name().equalsIgnoreCase(type);
    }

    /**
     * Checks if the param informed is a border.
     *
     * @param type param to be checked.
     * @return boolean.
     */
    public static boolean isBorder(final String type) {
        return BORDER.symbol.equals(type) || BORDER.name().equalsIgnoreCase(type);
    }

    /**
     * Checks if the param informed is land.
     *
     * @param type param to be checked.
     * @return boolean.
     */
    public static boolean isLand(final String type) {
        return LAND.symbol.equals(type) || LAND.name().equalsIgnoreCase(type);
    }

    /**
     * Checks if the param informed is a warrior.
     *
     * @param symbol param to be checked.
     * @return boolean.
     */
    public static boolean isWarrior(final String symbol) {
        return isPlayer(symbol) || isEnemy(symbol) || isAlly(symbol);
    }

    /**
     * Checks if the param informed is landscape.
     *
     * @param symbol param to be checked.
     * @return boolean.
     */
    public static boolean isLandscape(final String symbol) {
        return isSea(symbol) || isBorder(symbol) || isLand(symbol);
    }

    /**
     * Retrieves the Symbol for the param informed. if it isn't found returns Land.
     *
     * @param symbol param to be found.
     * @return the Symbol found.
     */
    public static Symbol getSymbol(final String symbol) {
        for (Symbol value : Symbol.values()) {
            if (value.name().equalsIgnoreCase(symbol) || value.symbol.equals(symbol)) {
                return value;
            }
        }

        return Symbol.LAND;
    }
}
