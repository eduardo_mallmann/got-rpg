package com.eduardomallmann.auto1.gotterminalrpg.commons.enums;

/**
 * Enum that holds the characters houses and its land type.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public enum Lands {

	LANNISTER("\u001B[101m\u001B[91m\u2593\u001B[0m", "Lannister"),
	ARRYN("\u001B[44m\u001B[34m\u2593\u001B[0m", "Arryn"),
	MARTELL("\u001B[105m\u001B[95m\u2593\u001B[0m", "Martell"),
	STARK("\u001B[107m\u001B[97m\u2593\u001B[0m", "Stark"),
	TARGARYEN("\u001B[45m\u001B[35m\u2593\u001B[0m", "Targaryen"),
	TULLY("\u001B[46m\u001B[36m\u2593\u001B[0m", "Tully"),
	TYRELL("\u001B[102m\u001B[92m\u2593\u001B[0m", "Tyrell"),
	BARATHEON("\u001B[103m\u001B[93m\u2593\u001B[0m", "Baratheon"),
	LANNISTERBARATHEON("\u001B[45m\u001B[35m\u2593\u001B[0m", "Lannister/Baratheon"),
	WALL("\u001B[100m\u001B[90m\u2593\u001B[0m", "Wall"),
	BEYONDTHEWALL("\u001B[102m\u001B[92m\u2593\u001B[0m", "BTW");

	private String land;
	private String name;

	/**
	 * Main constructor.
	 *
	 * @param land type of the land in the map.
	 * @param name name of the house in the character.
	 */
	Lands(final String land, final String name) {
		this.land = land;
		this.name = name;
	}

	/**
	 * Retrieves the Lands object by its name.
	 *
	 * @param name name to be found.
	 * @return HousesLand from its name.
	 */
	public static Lands retrieveByName(final String name) {
		for (Lands value : Lands.values()) {
			if (value.name.equalsIgnoreCase(name)) return value;
		}
		return null;
	}

	/**
	 * Retrieves the Lands object by its land.
	 *
	 * @param land symbol of the land.
	 * @return HousesLand from its name.
	 */
	public static Lands retrieveByLand(final String land) {
		for (Lands value : Lands.values()) {
			if (value.land.equalsIgnoreCase(land)) return value;
		}
		return null;
	}

	/**
	 * Retrieve the land type.
	 *
	 * @return the land type in string format.
	 */
	public String land() {
		return land;
	}

}
