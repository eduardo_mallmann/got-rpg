package com.eduardomallmann.auto1.gotterminalrpg.commons;

import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.TerminalColor;

import java.awt.*;
import java.awt.image.BufferedImage;

import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.getConfig;

/**
 * Interface responsible for the views commons interactions.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class ScreenService {

	/**
	 * Creates an image to be printed with the text informed.
	 *
	 * @param widthSize  width of the board.
	 * @param heightSize height of the board.
	 * @param phrase     phrase to be printed.
	 * @param marginLeft margin left of the printed text.
	 * @param marginTop  margin top of the printed text.
	 * @return BufferImage for the phrase informed.
	 */
	public static BufferedImage drawnTitle(int widthSize, int heightSize, String phrase, int marginLeft, int marginTop) {

		BufferedImage bufferedImage = new BufferedImage(widthSize, heightSize, BufferedImage.TYPE_INT_RGB);
		Graphics graphics = bufferedImage.getGraphics();

		Graphics2D graphics2D = (Graphics2D) graphics;
		graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		graphics2D.drawString(phrase, marginLeft, marginTop);

		return bufferedImage;
	}

	/**
	 * Prints the matrix in the console.
	 *
	 * @param screenMatrix matrix to be printed.
	 */
	public static void printScreen(String[][] screenMatrix) {
		printScreen(screenMatrix, Integer.valueOf(getConfig("screen.map.height")),
				Integer.valueOf(getConfig("screen.map.width")));
	}

	/**
	 * Prints the matrix in the console.
	 *
	 * @param screenMatrix matrix to be printed.
	 * @param height       the height of the printing.
	 * @param width        the width of the printing.
	 */
	public static void printScreen(String[][] screenMatrix, int height, int width) {

		for (int y = 0; y < height; y++) {

			StringBuilder stringBuilder = new StringBuilder();

			for (int x = 0; x < width; x++) {
				stringBuilder.append(screenMatrix[y][x]);
			}

			if (stringBuilder.toString().trim().isEmpty()) {
				continue;
			}

			System.out.println(stringBuilder);
		}
	}

	/**
	 * Prints the image in the console.
	 *
	 * @param image image to be printed.
	 */
	public static void printScreen(BufferedImage image) {

		for (int y = 0; y < image.getHeight(); y++) {

			StringBuilder stringBuilder = new StringBuilder();

			for (int x = 0; x < image.getWidth(); x++) {
				stringBuilder.append(image.getRGB(x, y) == -16777216 ? " "
						: TerminalColor.YELLOW.bold() + "*" + TerminalColor.RESET());
			}

			if (stringBuilder.toString().trim().isEmpty()) {
				continue;
			}

			System.out.println(stringBuilder);
		}
	}

}
