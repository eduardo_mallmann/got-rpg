package com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions;

import com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils;

/**
 * Class responsible for handle the game exceptions thrown when an exception occur.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class GameException extends Exception {

    private static final long serialVersionUID = -3935176288303612780L;

    private String errorKey;

    /**
     * Main constructor.
     *
     * @param errorKey key from config properties.
     */
    public GameException(final String errorKey) {
        super(ResourceUtils.getScript(errorKey));
        this.errorKey = errorKey;
    }

    public String getErrorKey() {
        return errorKey;
    }

    public void setErrorKey(final String errorKey) {
        this.errorKey = errorKey;
    }
}
