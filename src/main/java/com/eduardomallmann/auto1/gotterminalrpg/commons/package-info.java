/**
 * Provides the helper and common classes to the application.
 *
 * @since 1.0
 */
package com.eduardomallmann.auto1.gotterminalrpg.commons;