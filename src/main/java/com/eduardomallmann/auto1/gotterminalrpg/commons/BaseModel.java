package com.eduardomallmann.auto1.gotterminalrpg.commons;

import java.io.Serializable;

/**
 * Responsible for mapping all the models to be wrapped in its services.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public interface BaseModel extends Serializable, Cloneable {

}
