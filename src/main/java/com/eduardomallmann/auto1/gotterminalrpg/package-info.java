/**
 * Provides the classes and packages necessary to create the application.
 *
 * @since 1.0
 */
package com.eduardomallmann.auto1.gotterminalrpg;