package com.eduardomallmann.auto1.gotterminalrpg.characters;

import com.eduardomallmann.auto1.gotterminalrpg.commons.BaseModel;
import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.Profile;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Character information model. Handle the character data to be used and saved by the application.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class Character implements BaseModel {

	private static final long serialVersionUID = -5610340336569668517L;

	private String id;
	private String name;
	private String house;
	private Profile profile;
	private List<String> allies;
	private List<String> enemies;
	private boolean status;
	private Integer xPosition;
	private Integer yPosition;
	private Integer life;
	private List<Integer> damage;
	private List<Integer> armour;
	private BigDecimal experienceShared;
	private String lastLandPosition;

	/**
	 * Main constructor, implementing standard values.
	 */
	public Character() {
		this.allies = new ArrayList<>();
		this.enemies = new ArrayList<>();
		this.experienceShared = new BigDecimal("0.00");
	}

	/**
	 * Full constructor with all params.
	 *
	 * @param id               number identification in string format for register.
	 * @param name             name of the character.
	 * @param house            house that the character belongs.
	 * @param profile          informs the align of the character.
	 * @param allies           shows a list with the id of this character allies.
	 * @param enemies          shows a list with the id of this character enemies.
	 * @param status           informs whether the character is active.
	 * @param xPosition        the x position of the character in the westeros map.
	 * @param yPosition        the y position of the character in the westeros map.
	 * @param life             amount of character's life.
	 * @param damage           range of character's damage.
	 * @param armour           range of character's armour.
	 * @param experienceShared percentage of life, damage and armour to be shared in case of defeat or help.
	 * @param lastLandPosition informs the last land the character was for save purposes.
	 */
	public Character(String id, String name, String house, Profile profile, List<String> allies, List<String> enemies, boolean status,
					 Integer xPosition, Integer yPosition, Integer life, List<Integer> damage, List<Integer> armour,
					 BigDecimal experienceShared, String lastLandPosition) {
		this.id = id;
		this.name = name;
		this.house = house;
		this.profile = profile;
		this.allies = allies;
		this.enemies = enemies;
		this.status = status;
		this.xPosition = xPosition;
		this.yPosition = yPosition;
		this.life = life;
		this.damage = damage;
		this.armour = armour;
		this.experienceShared = experienceShared;
		this.lastLandPosition = lastLandPosition;
	}

	public static CharacterBuilder builder() {
		return new CharacterBuilder();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<String> getAllies() {
		return allies;
	}

	public void setAllies(List<String> allies) {
		this.allies = allies;
	}

	public List<String> getEnemies() {
		return enemies;
	}

	public void setEnemies(List<String> enemies) {
		this.enemies = enemies;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Integer getxPosition() {
		return xPosition;
	}

	public void setxPosition(Integer xPosition) {
		this.xPosition = xPosition;
	}

	public Integer getyPosition() {
		return yPosition;
	}

	public void setyPosition(Integer yPosition) {
		this.yPosition = yPosition;
	}

	public Integer getLife() {
		return life;
	}

	public void setLife(Integer life) {
		this.life = life;
	}

	public List<Integer> getDamage() {
		return damage;
	}

	public void setDamage(List<Integer> damage) {
		this.damage = damage;
	}

	public List<Integer> getArmour() {
		return armour;
	}

	public void setArmour(List<Integer> armour) {
		this.armour = armour;
	}

	public BigDecimal getExperienceShared() {
		return experienceShared;
	}

	public void setExperienceShared(BigDecimal experienceShared) {
		this.experienceShared = experienceShared;
	}

	public String getLastLandPosition() {
		return lastLandPosition;
	}

	public void setLastLandPosition(String lastLandPosition) {
		this.lastLandPosition = lastLandPosition;
	}

	@Override public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Character)) return false;
		Character character = (Character) o;
		return status == character.status &&
				id.equals(character.id) &&
				name.equals(character.name) &&
				house.equals(character.house) &&
				profile == character.profile &&
				allies.equals(character.allies) &&
				enemies.equals(character.enemies) &&
				xPosition.equals(character.xPosition) &&
				yPosition.equals(character.yPosition) &&
				life.equals(character.life) &&
				damage.equals(character.damage) &&
				armour.equals(character.armour) &&
				experienceShared.equals(character.experienceShared) &&
				Objects.equals(lastLandPosition, character.lastLandPosition);
	}

	@Override public int hashCode() {
		return Objects.hash(id, name, house, profile, allies, enemies, status, xPosition, yPosition, life, damage, armour, experienceShared,
				lastLandPosition);
	}

	@Override public String toString() {
		return "Character{" +
				"id=" + id +
				", name='" + name + '\'' +
				", house='" + house + '\'' +
				", profile=" + profile.build() +
				", allies=" + "[ " + String.join(", ", allies) + " ]" +
				", enemies=" + "[ " + String.join(", ", enemies) + " ]" +
				", status=" + status +
				", xPosition=" + xPosition +
				", yPosition=" + yPosition +
				", life=" + life +
				", damage=" + damage.stream().map(String::valueOf).collect(Collectors.joining("-")) +
				", armour=" + armour.stream().map(String::valueOf).collect(Collectors.joining("-")) +
				", experienceShared=" + experienceShared.toString() +
				", lastLandPosition=" + lastLandPosition +
				'}';
	}
}
