package com.eduardomallmann.auto1.gotterminalrpg.characters;

import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.Profile;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Builder pattern for Character object.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class CharacterBuilder {

	private Character character;

	/**
	 * Main constructor, instantiate a new character object.
	 */
	public CharacterBuilder() {
		character = new Character();
	}

	/**
	 * Inserts the id value of the Character.
	 *
	 * @param value identifier value.
	 * @return this
	 */
	public CharacterBuilder id(final String value) {
		this.character.setId(value);
		return this;
	}

	/**
	 * Inserts the name value of the Character.
	 *
	 * @param value name value.
	 * @return this
	 */
	public CharacterBuilder name(final String value) {
		this.character.setName(value);
		return this;
	}

	/**
	 * Inserts the house value of the Character.
	 *
	 * @param value house value.
	 * @return this
	 */
	public CharacterBuilder house(final String value) {
		this.character.setHouse(value);
		return this;
	}

	/**
	 * Inserts the Profile object for the Character.
	 *
	 * @param value profile of the character.
	 * @return this
	 */
	public CharacterBuilder profile(final Profile value) {
		if (null != value) this.character.setProfile(value);
		return this;
	}

	/**
	 * Inserts the Profile value in a String format of the Character.
	 *
	 * @param value Profile value in String format.
	 * @return this
	 */
	public CharacterBuilder profile(final String value) {
		if (null != value) this.character.setProfile(Profile.getProfile(value));
		return this;
	}

	/**
	 * Inserts the allies list for the Character.
	 *
	 * @param value allies list.
	 * @return this
	 */
	public CharacterBuilder allies(final List<String> value) {
		if (null != value) this.character.setAllies(value);
		return this;
	}

	/**
	 * Inserts the an ally in String format of the Character.
	 *
	 * @param value an ally in String format.
	 * @return this
	 */
	public CharacterBuilder allies(final String value) {
		if (null != value) this.character.setAllies(Arrays.asList(value.split("; ")));
		return this;
	}

	/**
	 * Inserts the enemies list of the Character.
	 *
	 * @param value identifier value.
	 * @return this
	 */
	public CharacterBuilder enemies(final List<String> value) {
		if (null != value) this.character.setEnemies(value);
		return this;
	}

	/**
	 * Inserts an enemy in String format of the Character.
	 *
	 * @param value an enemy in String format
	 * @return this
	 */
	public CharacterBuilder enemies(final String value) {
		if (null != value) this.character.setEnemies(Arrays.asList(value.split("; ")));
		return this;
	}

	/**
	 * Inserts the status of the Character.
	 *
	 * @param value status in a boolean object.
	 * @return this
	 */
	public CharacterBuilder status(final boolean value) {
		this.character.setStatus(value);
		return this;
	}

	/**
	 * Inserts the status in String format for the Character.
	 *
	 * @param value status in String format.
	 * @return this
	 */
	public CharacterBuilder status(final String value) {
		this.character.setStatus(Boolean.parseBoolean(value));
		return this;
	}

	/**
	 * Inserts the x position value of the Character.
	 *
	 * @param value the x position value.
	 * @return this
	 */
	public CharacterBuilder xPosition(final Integer value) {
		this.character.setxPosition(value);
		return this;
	}

	/**
	 * Inserts the x position value in String format for the Character.
	 *
	 * @param value the x position.
	 * @return this
	 */
	public CharacterBuilder xPosition(final String value) {
		this.character.setxPosition(Integer.valueOf(value));
		return this;
	}

	/**
	 * Inserts the y position value for the Character.
	 *
	 * @param value the y position.
	 * @return this
	 */
	public CharacterBuilder yPosition(final Integer value) {
		this.character.setyPosition(value);
		return this;
	}

	/**
	 * Inserts the y position in String format for the Character.
	 *
	 * @param value the y position.
	 * @return this
	 */
	public CharacterBuilder yPosition(final String value) {
		this.character.setyPosition(Integer.valueOf(value));
		return this;
	}

	/**
	 * Inserts the life value of the Character.
	 *
	 * @param value life value.
	 * @return this
	 */
	public CharacterBuilder life(final Integer value) {
		this.character.setLife(value);
		return this;
	}

	/**
	 * Inserts the life value in String format for the Character.
	 *
	 * @param value identifier value.
	 * @return this
	 */
	public CharacterBuilder life(final String value) {
		this.character.setLife(Integer.valueOf(value));
		return this;
	}

	/**
	 * Inserts the damage list of the Character.
	 *
	 * @param value damage list.
	 * @return this
	 */
	public CharacterBuilder damage(final List<Integer> value) {
		this.character.setDamage(value);
		return this;
	}

	/**
	 * Inserts the damage in String format of the Character.
	 *
	 * @param value damage in String format.
	 * @return this
	 */
	public CharacterBuilder damage(final String value) {
		this.character.setDamage(Arrays.asList(value.split("-")).stream().map(Integer::valueOf).collect(Collectors.toList()));
		return this;
	}

	/**
	 * Inserts the armour list of the Character.
	 *
	 * @param value armour list.
	 * @return this
	 */
	public CharacterBuilder armour(final List<Integer> value) {
		this.character.setArmour(value);
		return this;
	}

	/**
	 * Inserts the armour in Stirng format for the Character.
	 *
	 * @param value armour value.
	 * @return this
	 */
	public CharacterBuilder armour(final String value) {
		this.character.setArmour(Arrays.asList(value.split("-")).stream().map(Integer::valueOf).collect(Collectors.toList()));
		return this;
	}

	/**
	 * Inserts the experienceShared value of the Character.
	 *
	 * @param value experience value value.
	 * @return this
	 */
	public CharacterBuilder experienceShared(final BigDecimal value) {
		if (null != value) this.character.setExperienceShared(value);
		return this;
	}

	/**
	 * Inserts the experienceShared in String format for the Character.
	 *
	 * @param value experience value value.
	 * @return this
	 */
	public CharacterBuilder experienceShared(final String value) {
		if (null != value) this.character.setExperienceShared(new BigDecimal(value));
		return this;
	}

	/**
	 * Inserts the lastLandPosition value of the Character.
	 *
	 * @param value lastLandPosition value.
	 * @return this
	 */
	public CharacterBuilder lastLandPosition(final String value) {
		if (null != value) this.character.setLastLandPosition(value);
		return this;
	}

	/**
	 * Retrieves the Character instantiated.
	 *
	 * @return this
	 */
	public Character build() {
		return character;
	}

}
