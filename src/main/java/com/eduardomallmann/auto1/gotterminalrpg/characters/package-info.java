/**
 * Provides the necessary objects to handle characters.
 *
 * @since 1.0
 */
package com.eduardomallmann.auto1.gotterminalrpg.characters;