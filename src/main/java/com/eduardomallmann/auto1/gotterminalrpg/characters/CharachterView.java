package com.eduardomallmann.auto1.gotterminalrpg.characters;

import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.TerminalColor;

import java.util.List;
import java.util.stream.Collectors;

import static com.eduardomallmann.auto1.gotterminalrpg.commons.ScreenService.drawnTitle;
import static com.eduardomallmann.auto1.gotterminalrpg.commons.ScreenService.printScreen;
import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.getBoldScriptText;
import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.getScript;

/**
 * Responsible for the characters views.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class CharachterView {

	public static final int WIDTH_SIZE = 140;
	public static final int HEIGHT_SIZE = 10;
	public static final int MARGIN_TOP = 10;

	/**
	 * Prints in the terminal the page with the two fighters.
	 *
	 * @param opponent the opponent character.
	 * @param player   the player character.
	 */
	public static void printCharactersFightInfo(final Character opponent, final Character player) {

		System.out.println(TerminalColor.YELLOW.bold());
		printScreen(drawnTitle(WIDTH_SIZE, HEIGHT_SIZE, getScript("game.play.fight"), 0, MARGIN_TOP));
		System.out.println();

		System.out.println(getBoldScriptText("game.player"));
		System.out.println(getBoldScriptText("character.name").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(player.getName()));
		System.out.println(getBoldScriptText("character.house").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(player.getHouse()));
		System.out.println(getBoldScriptText("character.life").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(String.valueOf(player.getLife())));
		System.out.println(getBoldScriptText("character.damage").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(player.getDamage().stream().map(String::valueOf).collect(Collectors.joining("-"))));
		System.out.println(getBoldScriptText("character.armour").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(player.getArmour().stream().map(String::valueOf).collect(Collectors.joining("-"))));

		System.out.println();
		System.out.println(getBoldScriptText("game.opponent"));
		System.out.println(getBoldScriptText("character.name").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(opponent.getName()));
		System.out.println(getBoldScriptText("character.house").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(opponent.getHouse()));
		System.out.println(getBoldScriptText("character.life").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(String.valueOf(opponent.getLife())));
		System.out.println(getBoldScriptText("character.damage").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(opponent.getDamage().stream().map(String::valueOf).collect(Collectors.joining("-"))));
		System.out.println(getBoldScriptText("character.armour").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(opponent.getArmour().stream().map(String::valueOf).collect(Collectors.joining("-"))));

	}

	/**
	 * Prints in the terminal the page with the list of characters.
	 *
	 * @param characters list to be printed.
	 */
	public void printCharacterListMenu(final List<Character> characters) {

		printScreen(drawnTitle(80, 10, getScript("character.title"), 0, 10));
		System.out.println();
		System.out.println(getBoldScriptText("character.choice"));
		System.out.println();

		characters.forEach(character -> System.out.println(character.getId().concat(". ").concat(character.getName())));

		System.out.println();
		System.out.println(getBoldScriptText("character.choice.back"));
	}

	/**
	 * Prints in the terminal the page with the characters info.
	 *
	 * @param character object to be printed.
	 */
	public void printCharacterInfo(final Character character) {

		System.out.println(TerminalColor.YELLOW.bold());
		printScreen(drawnTitle(140, 10, character.getName(), 0, 10));
		System.out.println();

		System.out.println(getBoldScriptText("character.name").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(character.getName()));

		System.out.println(getBoldScriptText("character.house").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(character.getHouse()));

		System.out.println(getBoldScriptText("character.life").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(String.valueOf(character.getLife())));

		System.out.println(getBoldScriptText("character.damage").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(character.getDamage().stream().map(String::valueOf).collect(Collectors.joining("-"))));

		System.out.println(getBoldScriptText("character.armour").concat(": ").concat(TerminalColor.YELLOW.bold())
				.concat(character.getArmour().stream().map(String::valueOf).collect(Collectors.joining("-"))));

		System.out.println(getBoldScriptText("character.allies").concat(": ").concat(TerminalColor.YELLOW.bold()));

		character.getAllies().forEach(ally -> System.out.println("       ".concat(ally)));

		System.out.println(getBoldScriptText("character.enemies").concat(": ").concat(TerminalColor.YELLOW.bold()));

		character.getEnemies().forEach(enemy -> System.out.println("        ".concat(enemy)));

		System.out.println();
		System.out.println(getBoldScriptText("character.view.scape"));
	}

}
