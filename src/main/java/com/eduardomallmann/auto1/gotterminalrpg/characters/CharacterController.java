package com.eduardomallmann.auto1.gotterminalrpg.characters;

import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.getScriptAlert;

/**
 * Responsible for the handle the interactions between characters views and dao.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class CharacterController {

	private static CharacterService characterService;
	private static CharachterView charachterView;
	private static Scanner scanner;

	/**
	 * Main constructor, instantiates the needed services.
	 */
	public CharacterController() {
		characterService = new CharacterService();
		charachterView = new CharachterView();
		scanner = new Scanner(System.in);
	}

	/**
	 * Prints in the terminal the page with the list of characters to choose.
	 *
	 * @return The chosen character.
	 */
	public Character charactersChoice() {

		List<Character> characters = new ArrayList<>();

		try {
			characters = characterService.retrieveAll();
		} catch (GameException e) {
			System.out.println(getScriptAlert(e.getErrorKey()));
		}

		charachterView.printCharacterListMenu(characters);

		String characterId = scanner.nextLine();

		return characters.stream().filter(ch -> ch.getId().equals(characterId)).findFirst().orElse(null);
	}

	/**
	 * Prints the character informed in the param.
	 *
	 * @param character the character to be printed.
	 */
	public void printCharacter(Character character) {

		charachterView.printCharacterInfo(character);

		scanner.nextLine();
	}

}
