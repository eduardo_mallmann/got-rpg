package com.eduardomallmann.auto1.gotterminalrpg.characters;

import com.eduardomallmann.auto1.gotterminalrpg.commons.BaseService;
import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.Profile;
import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;
import com.eduardomallmann.auto1.gotterminalrpg.commons.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.getConfig;

/**
 * Responsible for the business and data accessing layers of the characters.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class CharacterService implements BaseService<Character> {

	public static final String CHARACTER_FILE_NAME = getConfig("character.file.name");

	/**
	 * Retrieve the list of characters of the game.
	 *
	 * @return a list of characters.
	 * @throws GameException when the list can't be retrieved.
	 */
	public List<Character> retrieveAll() throws GameException {

		Path filePath = Paths.get(this.getClass()
				.getResource(File.separator +
						getConfig("files.folder") +
						File.separator +
						CHARACTER_FILE_NAME)
				.getFile());

		if (!Files.exists(filePath)) return new ArrayList<>();

		try (Stream<String> stream = Files.lines(filePath)) {

			return stream.skip(1L).parallel().map(this::unwrapCSV).collect(Collectors.toList());

		} catch (IOException e) {
			throw new GameException("error.character.list");
		}
	}

	/**
	 * Save the list of characters for the user when save the game.
	 *
	 * @param characters characters list to be saved.
	 * @param username   name of the user of the game.
	 * @param gameName   name of the game saved.
	 * @throws GameException when the save failed.
	 */
	public void saveAll(final List<Character> characters, final String username, final String gameName) throws GameException {

		List<String> usersCSV = characters.parallelStream().map(this::wrapCSV).collect(Collectors.toList());

		FileUtils.saveFile(username.concat("_").concat(gameName).concat("_").concat(CHARACTER_FILE_NAME), usersCSV);
	}

	/**
	 * Retrieves the list of characteres from a saved game.
	 *
	 * @param username user whom saves the game.
	 * @param gameName name of the game saved.
	 * @return list of characters.
	 * @throws GameException when a problem occur with the operation.
	 */
	public List<Character> retrieveSavedGame(final String username, final String gameName) throws GameException {

		Path filePath = Paths.get(FileUtils.SYSTEM_FOLDER_PATH.concat(File.separator).concat(username).concat("_").concat(gameName)
				.concat("_").concat(CHARACTER_FILE_NAME));

		if (!Files.exists(filePath)) throw new GameException("error.saved.game");

		try (Stream<String> stream = Files.lines(filePath)) {

			return stream.parallel().map(this::unwrapCSV).collect(Collectors.toList());

		} catch (IOException e) {
			throw new GameException("error.saved.game.list");
		}

	}

	/**
	 * Set the profile of the characters to initiate the game, based on the character choose to play.
	 *
	 * @param player character choose to play.
	 */
	public List<Character> buildCharactersGameProfile(Character player) throws GameException {

		player.setProfile(Profile.PLAYER);

		List<Character> characters = new ArrayList<>();
		characters.add(player);

		for (Character character : retrieveAll()) {
			if (character != player) {
				if (player.getAllies().contains(character.getName())) {
					character.setProfile(Profile.ALLY);
				} else {
					character.setProfile(Profile.ENEMY);
				}
				characters.add(character);
			}
		}
		return characters;
	}

	/**
	 * Unwraps the Character from the csv line.
	 *
	 * @param csvLine one line from csv file.
	 * @return Character instance from the csv line.
	 */
	@Override
	public Character unwrapCSV(String csvLine) {

		String[] data = csvLine.split(",");

		return Character.builder()
				.id(changeToNull(data[0].trim()))
				.name(changeToNull(data[1].trim()))
				.house(changeToNull(data[2].trim()))
				.profile(changeToNull(data[3].trim()))
				.allies(changeToNull(data[4].trim()))
				.enemies(changeToNull(data[5].trim()))
				.status(changeToNull(data[6].trim()))
				.xPosition(changeToNull(data[7].trim()))
				.yPosition(changeToNull(data[8].trim()))
				.life(changeToNull(data[9].trim()))
				.damage(changeToNull(data[10].trim()))
				.armour(changeToNull(data[11].trim()))
				.experienceShared(changeToNull(data[12].trim()))
				.lastLandPosition(changeToNull(data[13].trim()))
				.build();
	}

	/**
	 * Verifies and change the String null value to actually null value.
	 *
	 * @param value value to be verified.
	 * @return the real value of the string.
	 */
	private String changeToNull(String value) {
		if ("null".equalsIgnoreCase(value)) {
			return null;
		} else {
			return value;
		}
	}

	/**
	 * Wraps the Character in a String with csv format.
	 *
	 * @return String in CSV format.
	 */
	@Override
	public String wrapCSV(Character object) {

		String allies = null == object.getAllies() || object.getAllies().isEmpty() ? "" : String.join("; ", object.getAllies());
		String enemies= null == object.getEnemies() || object.getEnemies().isEmpty() ? "" : String.join("; ", object.getEnemies());
		String damage = object.getDamage().stream().map(String::valueOf).collect(Collectors.joining("-"));
		String armour = object.getArmour().stream().map(String::valueOf).collect(Collectors.joining("-"));

		return object.getId().concat(", ")
				.concat(object.getName()).concat(", ")
				.concat(object.getHouse()).concat(", ")
				.concat(object.getProfile().build()).concat(", ")
				.concat(allies).concat(", ")
				.concat(enemies).concat(", ")
				.concat(String.valueOf(object.isStatus())).concat(", ")
				.concat(String.valueOf(object.getxPosition())).concat(", ")
				.concat(String.valueOf(object.getyPosition())).concat(", ")
				.concat(String.valueOf(object.getLife())).concat(", ")
				.concat(damage).concat(", ")
				.concat(armour).concat(", ")
				.concat(String.valueOf(object.getExperienceShared())).concat(", ")
				.concat(null == object.getLastLandPosition() ? "" : object.getLastLandPosition());
	}
}
