package com.eduardomallmann.auto1.gotterminalrpg.users;

import com.eduardomallmann.auto1.gotterminalrpg.commons.BaseService;
import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;
import com.eduardomallmann.auto1.gotterminalrpg.commons.utils.FileUtils;
import com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Responsible to handle the business logic and data access to the operational system.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class UserService implements BaseService<User> {

	public static final String USER_FILE_NAME = ResourceUtils.getConfig("user.file.name");

	/**
	 * Retrieve the list of users of the game.
	 *
	 * @return a list of users.
	 * @throws GameException when the list can't be retrieved.
	 */
	public List<User> retrieveAll() throws GameException {

		Path filePath = Paths.get(FileUtils.SYSTEM_FOLDER_PATH.concat(File.separator).concat(USER_FILE_NAME));

		if (!Files.exists(filePath)) return new ArrayList<>();

		try (Stream<String> stream = Files.lines(filePath)) {

			return stream.parallel().map(this::unwrapCSV).collect(Collectors.toList());

		} catch (IOException e) {
			throw new GameException("error.user.list");
		}
	}

	/**
	 * Save the list of users.
	 *
	 * @param users user list to be saved.
	 * @throws GameException when the save failed.
	 */
	public void saveAll(final List<User> users) throws GameException {

		List<String> usersCSV = users.parallelStream().map(this::wrapCSV).collect(Collectors.toList());

		FileUtils.saveFile(USER_FILE_NAME, usersCSV);
	}

	/**
	 * Unwraps the user from the csv line.
	 *
	 * @param csvLine one line from csv file.
	 * @return User instance from the csv line.
	 */
	public User unwrapCSV(final String csvLine) {

		String[] data = csvLine.split(", ");

		User user = new User(data[0], data[1]);

		if (data.length == 3) {

			String[] games = data[2].split("; ");

			user.getGames().addAll(Arrays.asList(games));
		}

		return user;
	}

	/**
	 * Wraps the object in a String with csv format.
	 *
	 * @return String in CSV format.
	 */
	public String wrapCSV(final User user) {

		String gamesString = "";

		if (!user.getGames().isEmpty()) {
			gamesString = String.join("; ", user.getGames());
		}

		return user.getUsername().concat(", ").concat(user.getPassword()).concat(", ").concat(gamesString);
	}
}
