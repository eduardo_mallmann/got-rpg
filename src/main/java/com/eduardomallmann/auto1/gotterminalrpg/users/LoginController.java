package com.eduardomallmann.auto1.gotterminalrpg.users;

import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.TerminalColor;
import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;

import java.util.List;
import java.util.Scanner;

import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.getScript;

/**
 * Responsible for control the view interactions.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class LoginController {

	private UserService userService;

	/**
	 * Main constructor, instantiate the userService.
	 */
	public LoginController() {
		this.userService = new UserService();
	}

	/**
	 * Controls the login page actions.
	 *
	 * @param users list of system users.
	 * @return the user loged.
	 */
	public User loginViewController(List<User> users) {

		LoginView view = new LoginView();

		view.printLoginScreen();

		Scanner scanner = new Scanner(System.in);

		String username = scanner.nextLine().trim();

		if (username.contains(" ")) {
			while (username.contains(" ")) {
				view.printAlertUserName();
				username = scanner.nextLine().trim();
			}
		}

		final String usernameTypeSafe = username;

		User user = users.stream().filter(userFromList -> userFromList.getUsername().equalsIgnoreCase(usernameTypeSafe))
				.findFirst().orElse(null);

		if (null == user) {
			System.out.println(TerminalColor.RED.bold().concat(getScript("login.new.user"))
					.concat(TerminalColor.RESET()));
		}

		view.printPassword();
		String password = scanner.nextLine();

		if (null != user) {

			while (!user.getPassword().equals(password)) {
				view.printAlertPassword();
				password = scanner.nextLine();
				System.out.println();
			}

			return user;

		} else {

			user = new User(usernameTypeSafe, password);
			users.add(user);

			try {
				userService.saveAll(users);
			} catch (GameException e) {
				view.printSaveExceptionMessage(e);
			}

			return user;
		}
	}

	/**
	 * Retrieve all users from the application.
	 *
	 * @return a list of users.
	 * @throws GameException when the retrieval fails.
	 */
	public List<User> retrieveAllUsers() throws GameException {
		return userService.retrieveAll();
	}
}
