package com.eduardomallmann.auto1.gotterminalrpg.users;

import com.eduardomallmann.auto1.gotterminalrpg.commons.BaseModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * User information model. Handle the user data to be saved by the application.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class User implements BaseModel {

    private static final long serialVersionUID = -5526265697053884759L;

    private String username;
    private String password;
    private List<String> games;

    /**
     * Main constructor.
     */
    public User() {
    }

    /**
     * Optional constructor.
     *
     * @param username name of the user.
     * @param password user's password.
     */
    public User(final String username, final String password) {
        this.username = username;
        this.password = password;
        this.games = new ArrayList<>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public List<String> getGames() {
        return games;
    }

    public void setGames(final List<String> games) {
        this.games = games;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return username.equals(user.username) &&
                password.equals(user.password) &&
                Objects.equals(games, user.games);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, games);
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", games=" + String.join(", ", games) +
                '}';
    }
}
