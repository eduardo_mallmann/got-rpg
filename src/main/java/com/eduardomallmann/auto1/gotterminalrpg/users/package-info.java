/**
 * Provides the necessary objects to handle users login and saved games.
 *
 * @since 1.0
 */
package com.eduardomallmann.auto1.gotterminalrpg.users;