package com.eduardomallmann.auto1.gotterminalrpg.users;

import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.TerminalColor;
import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;

import java.awt.image.BufferedImage;

import static com.eduardomallmann.auto1.gotterminalrpg.commons.ScreenService.drawnTitle;
import static com.eduardomallmann.auto1.gotterminalrpg.commons.ScreenService.printScreen;
import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.*;

/**
 * Responsible to handle the login view, first screen of the game.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class LoginView {


	/**
	 * Prints the welcome page.
	 */
	public void printLoginScreen() {

		BufferedImage welcomeScreen = drawnTitle(140, 50, getScript("login.welcome.screen"), 35, 20);
		BufferedImage gotScreen = drawnTitle(140, 50, getScript("login.got.screen"), 20, 20);
		BufferedImage arcadeRPGScreen = drawnTitle(140, 50, getScript("login.arcade.rpg"), 31, 20);

		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println(TerminalColor.YELLOW.bold());
		printScreen(welcomeScreen);
		System.out.println();
		printScreen(gotScreen);
		System.out.println();
		printScreen(arcadeRPGScreen);
		System.out.println();
		System.out.println();
		System.out.println(getBoldScriptText("login.text.info"));
		System.out.print(getScript("login.username"));

	}

	/**
	 * Prints the username typing alert.
	 */
	public void printAlertUserName() {
		System.out.println(getScriptAlert("login.username.not.valid"));
		System.out.print(getScript("login.username"));
	}

	/**
	 * Prints the password.
	 */
	public void printPassword() {
		System.out.print(getScript("login.password"));

	}

	/**
	 * Prints the password matching alert.
	 */
	public void printAlertPassword() {
		System.out.println(getScriptAlert("login.password.not.match"));
		System.out.print(getScript("login.password"));
	}

	/**
	 * Prints the user save alert, when new user didn't save.
	 */
	public void printSaveExceptionMessage(GameException e) {
		System.out.println();
		System.out.println(getScriptAlert(e.getErrorKey()));
	}

}