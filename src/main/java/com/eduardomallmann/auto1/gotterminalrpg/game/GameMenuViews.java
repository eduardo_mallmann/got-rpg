package com.eduardomallmann.auto1.gotterminalrpg.game;

import com.eduardomallmann.auto1.gotterminalrpg.commons.ScreenService;
import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.Symbol;

import java.util.List;

import static com.eduardomallmann.auto1.gotterminalrpg.commons.ScreenService.drawnTitle;
import static com.eduardomallmann.auto1.gotterminalrpg.commons.ScreenService.printScreen;
import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.getBoldScriptText;
import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.getScript;

/**
 * Responsible for the game menu views.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class GameMenuViews {

	/**
	 * Prints in the terminal the initial game menu page.
	 */
	public static void printInitialMenu() {

		printScreen(drawnTitle(140, 50, getScript("login.got.screen"), 0, 20));
		System.out.println();
		System.out.println(getBoldScriptText("game.menu.initial"));
		System.out.println();
		System.out.println(getScript("game.menu.new"));
		System.out.println(getScript("game.menu.resume"));
		System.out.println(getScript("game.menu.explore"));
		System.out.println();
		System.out.println(getBoldScriptText("game.exit.choice"));
	}

	/**
	 * Prints in the terminal the proceed message, made to be reused by various menus.
	 */
	public static void printProceedMessage() {
		System.out.println();
		System.out.println(getBoldScriptText("game.menu.proceed"));
		System.out.println();
	}

	/**
	 * Prints in the terminal the message in the param.
	 *
	 * @param messageKey key to the message to be printed.
	 */
	public static void printMessage(final String messageKey) {
		System.out.println();
		System.out.println(getBoldScriptText(messageKey));
		System.out.println();
	}

	/**
	 * Prints in the terminal the resume game menu.
	 *
	 * @param gamesSaved list of game names.
	 */
	public static void printResumeGameMenu(final List<String> gamesSaved) {

		printScreen(drawnTitle(140, 50, getScript("game.menu.resume.title"), 0, 20));
		System.out.println();
		System.out.println(getBoldScriptText("game.menu.resume.initial"));
		System.out.println();
		for (int i = 0; i < gamesSaved.size(); i++) {
			System.out.println(String.valueOf(i + 1).concat(". ").concat(gamesSaved.get(i)));
		}
	}

	/**
	 * Prints in the terminal the explore game menu.
	 */
	public static void printExploreGameMenu() {

		printScreen(drawnTitle(140, 50, getScript("game.menu.explore.title"), 0, 20));
		System.out.println();
		System.out.println(getBoldScriptText("game.menu.initial"));
		System.out.println();
		System.out.println(getScript("game.menu.explore.characters"));
		System.out.println(getScript("game.menu.explore.instructions"));
		System.out.println();
		System.out.println(getBoldScriptText("character.choice.back"));
	}

	/**
	 * Prints in the terminal the fight enemy menu.
	 */
	public static void printEnemyFightMenu() {

		System.out.println();
		System.out.println(getBoldScriptText("game.play.fight.action"));
		System.out.println();
		System.out.println("1. ".concat(getScript("game.play.fight.action.enemy.attack")));
		System.out.println("2. ".concat(getScript("game.play.fight.action.enemy.ally")));
		System.out.println();
		System.out.println(getBoldScriptText("character.choice.back"));

	}

	/**
	 * Prints in the terminal the fight ally menu.
	 */
	public static void printAllyFightMenu() {

		System.out.println();
		System.out.println(getBoldScriptText("game.play.fight.action"));
		System.out.println();
		System.out.println("1. ".concat(getScript("game.play.fight.action.ally.attack")));
		System.out.println("2. ".concat(getScript("game.play.fight.action.ally.ally")));
		System.out.println();
		System.out.println(getBoldScriptText("character.choice.back"));

	}

	/**
	 * Prints in the terminal the game instructions menu from explore.
	 */
	public static void printGameInstructionsMenu() {

		printScreen(drawnTitle(140, 50, getScript("game.menu.instructions"), 0, 20));
		System.out.println();
		System.out.println(getBoldScriptText("game.menu.initial"));
		System.out.println();
		System.out.println("1. ".concat(getScript("game.menu.instructions.map.title")));
		System.out.println("2. ".concat(getScript("game.menu.instructions.command.title")));
		System.out.println();
		System.out.println(getBoldScriptText("character.choice.back"));
	}

	/**
	 * Prints in the terminal the map instructions page.
	 */
	public static void printMapInstructions() {

		printScreen(drawnTitle(140, 50, getScript("game.menu.instructions.map.title"), 0, 20));

		System.out.println();
		System.out.println(getBoldScriptText("game.menu.instructions.map.form"));
		System.out.print("        ");
		System.out.println(getScript("game.menu.instructions.map.form.info"));
		System.out.print("        ");
		System.out.println(getScript("game.menu.instructions.map.form.info2"));
		System.out.print("        ");
		System.out.println(getScript("game.menu.instructions.map.form.info3"));

		System.out.println();
		System.out.println(getBoldScriptText("game.menu.instructions.map.characters"));
		System.out.print("        ");
		System.out.println(getScript("game.menu.instructions.map.characters.info"));
		System.out.print("        ");
		System.out.println(getScript("game.menu.instructions.map.characters.info2"));

		System.out.println();
		System.out.println(getBoldScriptText("game.menu.instructions.map.characters.profiles"));
		System.out.println();
		System.out.print("        ");
		System.out.print(getScript("game.menu.instructions.mark"));
		System.out.print(" ");
		System.out.println(getScript("game.menu.instructions.map.characters.profiles-player"));
		System.out.print("           ");
		System.out.print(getScript("game.menu.instructions.map.characters.profiles-symbol").concat(":"));
		System.out.print("  ");
		System.out.println(Symbol.PLAYER.build());
		System.out.print("           ");
		System.out.print(getScript("game.menu.instructions.map.characters.profiles-info").concat(":"));
		System.out.print(" ");
		System.out.println(getScript("game.menu.instructions.map.characters.profiles-player-info"));
		System.out.println();
		System.out.print("        ");
		System.out.print(getScript("game.menu.instructions.mark"));
		System.out.print(" ");
		System.out.println(getScript("game.menu.instructions.map.characters.profiles-ally"));
		System.out.print("           ");
		System.out.print(getScript("game.menu.instructions.map.characters.profiles-symbol").concat(":"));
		System.out.print("  ");
		System.out.println(Symbol.ALLY.build());
		System.out.print("           ");
		System.out.print(getScript("game.menu.instructions.map.characters.profiles-info").concat(":"));
		System.out.print(" ");
		System.out.println(getScript("game.menu.instructions.map.characters.profiles-ally-info"));
		System.out.print("                 ");
		System.out.println(getScript("game.menu.instructions.map.characters.profiles-ally-info2"));
		System.out.print("                 ");
		System.out.println(getScript("game.menu.instructions.map.characters.profiles-ally-info3"));
		System.out.println();
		System.out.print("        ");
		System.out.print(getScript("game.menu.instructions.mark"));
		System.out.print(" ");
		System.out.println(getScript("game.menu.instructions.map.characters.profiles-enemy"));
		System.out.print("           ");
		System.out.print(getScript("game.menu.instructions.map.characters.profiles-symbol").concat(":"));
		System.out.print("  ");
		System.out.println(Symbol.ENEMY.build());
		System.out.print("           ");
		System.out.print(getScript("game.menu.instructions.map.characters.profiles-info").concat(":"));
		System.out.print(" ");
		System.out.println(getScript("game.menu.instructions.map.characters.profiles-enemy-info"));
		System.out.print("                 ");
		System.out.println(getScript("game.menu.instructions.map.characters.profiles-enemy-info2"));
		System.out.println();
		System.out.print("        ");
		System.out.print(getScript("game.menu.instructions.mark"));
		System.out.print(" ");
		System.out.println(getScript("game.menu.instructions.map.characters.profiles-defeated"));
		System.out.print("           ");
		System.out.print(getScript("game.menu.instructions.map.characters.profiles-symbol").concat(":"));
		System.out.print("  ");
		System.out.println(Symbol.DEFEATED.build());
		System.out.print("           ");
		System.out.print(getScript("game.menu.instructions.map.characters.profiles-info").concat(":"));
		System.out.print(" ");
		System.out.println(getScript("game.menu.instructions.map.characters.profiles-defeated-info"));
	}

	/**
	 * Prints in the terminal the command instructions page.
	 */
	public static void printCommandInstructions() {
		printScreen(drawnTitle(140, 50, getScript("game.menu.instructions.command.title"), 0, 20));
		System.out.println();
		System.out.println(getBoldScriptText("game.menu.instructions.command.moves"));
		System.out.println();
		System.out.println(getScript("game.menu.instructions.command.moves.left"));
		System.out.println(getScript("game.menu.instructions.command.moves.right"));
		System.out.println(getScript("game.menu.instructions.command.moves.up"));
		System.out.println(getScript("game.menu.instructions.command.moves.down"));

		System.out.println();
		System.out.println(getBoldScriptText("game.menu.instructions.command.menu"));
		System.out.println();
		System.out.println(getScript("game.menu.instructions.command.exit"));
		System.out.println(getScript("game.menu.instructions.command.save"));
		System.out.println(getScript("game.menu.instructions.command.instructions.map"));
		System.out.println(getScript("game.menu.instructions.command.instructions.command"));
	}

	/**
	 * Prints a big message in the terminal.
	 *
	 * @param messageKey phrase or word to be printed.
	 */
	public static void printBigMessage(String messageKey) {
		ScreenService.printScreen(drawnTitle(140, 50, getScript(messageKey), 35, 20));
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println(getBoldScriptText("game.menu.goback"));
	}

}
