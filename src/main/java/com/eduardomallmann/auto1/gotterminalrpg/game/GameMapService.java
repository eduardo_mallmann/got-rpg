package com.eduardomallmann.auto1.gotterminalrpg.game;

import com.eduardomallmann.auto1.gotterminalrpg.characters.Character;
import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.Lands;
import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.Symbol;
import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.TerminalColor;
import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.getConfig;

/**
 * Responsible to handle the game map services.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class GameMapService {

	private Integer width, height;
	private String[][] screenMatrix;
	private String previousSimbol;

	/**
	 * Main constructor, instantiate the class with default data.
	 */
	public GameMapService() {
		width = Integer.valueOf(getConfig("screen.map.width"));
		height = Integer.valueOf(getConfig("screen.map.height"));
		this.screenMatrix = new String[height][width];
	}

	/**
	 * Prepares the game map to be printed.
	 *
	 * @param characters list of characters to be disposed in the map.
	 * @throws GameException when a problem occur when retrieving the symbol.
	 */
	public void assemblyScreenMap(final List<Character> characters) throws GameException {

		for (int i = 0; i < this.height; i++) {
			for (int j = 0; j < this.width; j++) {
				this.screenMatrix[i][j] = this.findSymbol(characters, this.retrieveScreenPixels(), i, j);
			}
		}
	}

	/**
	 * Retrive the symbol for that piece of the map.
	 *
	 * @param characters   list of characters
	 * @param screenPixels list with each piece of the map.
	 * @param height       y position.
	 * @param width        x position.
	 * @return the symbol of that piece of the map.
	 * @throws GameException when the symbol is not found.
	 */
	private String findSymbol(final List<Character> characters, final List<ScreenPixel> screenPixels, final int height,
							  final int width) throws GameException {

		Optional<Character> character = characters.stream()
				.filter(ch -> width == ch.getxPosition() && height == ch.getyPosition())
				.findFirst();

		String symbol;

		if (character.isPresent()) {
			symbol = Symbol.getSymbol(character.get().getProfile().build()).build();
		} else {
			symbol = screenPixels.stream().filter(pixel -> pixel.getX() == width && pixel.getY() == height)
					.findFirst().orElseThrow(() -> new GameException("error.map.building.symbol")).getSymbol();
		}
		return symbol;
	}

	/**
	 * Retrieves the screen object map from the map file.
	 *
	 * @return list with the pieces information.
	 */
	private List<ScreenPixel> retrieveScreenPixels() {

		List<ScreenPixel> screenPixels = new ArrayList<>();

		String filePath = this.getClass()
				.getResource(File.separator +
						getConfig("files.folder") +
						File.separator +
						getConfig("screen.map.file"))
				.getFile();

		try (Stream<String> stream = Files.lines(Paths.get(filePath))) {

			screenPixels.addAll(stream.skip(1).map(this::unwrapCSV)
					.collect(Collectors.toList()));

		} catch (NullPointerException | IOException e) {
			e.printStackTrace();
		}

		return screenPixels;
	}

	/**
	 * Unwraps the CSV file to ScreenPixel object.
	 *
	 * @param csvLine line of CSV from the file.
	 * @return the ScreenPixel object.
	 */
	private ScreenPixel unwrapCSV(String csvLine) {

		String[] splitedLine = csvLine.split(",");

		return this.builder(Integer.valueOf(splitedLine[0]), Integer.valueOf(splitedLine[1]), splitedLine[2], splitedLine[3]);
	}

	/**
	 * Instantiate the ScreenPixel with the retrieved data.
	 *
	 * @param y     y positition of the piece.
	 * @param x     x positition of the piece.
	 * @param color name of the color.
	 * @param type  type of the piece.
	 * @return the ScreenPixel object.
	 */
	private ScreenPixel builder(int y, int x, String color, String type) {

		String backgroundColor = "";
		String textColor = "";

		if (Symbol.isLandscape(type)) {
			textColor = TerminalColor.getColor(color).build();
			backgroundColor = TerminalColor.getBKColor(color).build();
		}

		String symbol = backgroundColor + textColor + Symbol.getSymbol(type).build() + TerminalColor.RESET();

		return new ScreenPixel(x, y, symbol);
	}

	/**
	 * Sets the location of the character when moving on the map. And kept its previous symbol.
	 *
	 * @param xPosition position x.
	 * @param yPosition position y.
	 */
	public void setCharacterOnMapLocation(int xPosition, int yPosition) {
		this.previousSimbol = this.screenMatrix[yPosition][xPosition];
		this.screenMatrix[yPosition][xPosition] = Symbol.PLAYER.build();
	}

	/**
	 * Set back the symbol for the previous location of the character on the map.
	 *
	 * @param xPosition position x.
	 * @param yPosition position y.
	 * @param character player
	 */
	public void setPreviousSymbolOnMap(int xPosition, int yPosition, Character character) {
		if (null == previousSimbol && null == character.getLastLandPosition()) {
			this.screenMatrix[yPosition][xPosition] = Lands.retrieveByName(character.getHouse()).land();
		} else if (null == previousSimbol && null != character.getLastLandPosition()) {
			this.screenMatrix[yPosition][xPosition] = Lands.retrieveByLand(character.getLastLandPosition()).land();
		} else {
			this.screenMatrix[yPosition][xPosition] = this.previousSimbol;
		}
	}

	/**
	 * Retrieves the symbol from coordinates informed.
	 *
	 * @param xPosition position x.
	 * @param yPosition position y.
	 */
	public String retrieveSymbolFromMap(int xPosition, int yPosition) {
		return this.screenMatrix[yPosition][xPosition];
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String[][] getScreenMatrix() {
		return screenMatrix;
	}

	public void setScreenMatrix(String[][] screenMatrix) {
		this.screenMatrix = screenMatrix;
	}

	public String getPreviousSimbol() {
		return previousSimbol;
	}

	public void setPreviousSimbol(String previousSimbol) {
		this.previousSimbol = previousSimbol;
	}
}
