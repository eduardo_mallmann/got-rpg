package com.eduardomallmann.auto1.gotterminalrpg.game;

import com.eduardomallmann.auto1.gotterminalrpg.characters.CharachterView;
import com.eduardomallmann.auto1.gotterminalrpg.characters.Character;
import com.eduardomallmann.auto1.gotterminalrpg.characters.CharacterController;
import com.eduardomallmann.auto1.gotterminalrpg.characters.CharacterService;
import com.eduardomallmann.auto1.gotterminalrpg.commons.ScreenService;
import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.Profile;
import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.Symbol;
import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;
import com.eduardomallmann.auto1.gotterminalrpg.users.User;
import com.eduardomallmann.auto1.gotterminalrpg.users.UserService;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.getBoldScriptText;
import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.getScriptAlert;

/**
 * Responsible to manage the game.
 *
 * @author Eduardo Mallmann
 * @since 1.0
 */
public class GameController {

	private final static Integer ALLY_SHARED_EXPERIENCE_DIVISOR = 2;

	private static Scanner scanner = new Scanner(System.in);
	private static List<User> userList;
	private static User userPrincipal;

	/**
	 * Manage the game starting.
	 *
	 * @param users list of users of the application.
	 * @param user  the user logged.
	 */
	public static void start(final List<User> users, final User user) {

		userList = users;
		userPrincipal = user;

		GameMenuViews.printInitialMenu();

		String option = scanner.nextLine();

		switch (option) {
		case "1":
			newGame();
			break;
		case "2":
			resumeGame();
			break;
		case "3":
			explore();
			break;
		default:
			exit();
		}
	}

	/**
	 * Manage the new game feature.
	 */
	private static void newGame() {

		CharacterController characterController = new CharacterController();

		Character character = characterController.charactersChoice();

		if (null == character) start(userList, userPrincipal);

		CharacterService characterService = new CharacterService();
		List<Character> characters = new ArrayList<>();

		try {
			characters.addAll(characterService.buildCharactersGameProfile(character));
		} catch (GameException e) {
			System.out.println(getScriptAlert(e.getErrorKey()));
			start(userList, userPrincipal);
		}

		GameMapService gameMapService = new GameMapService();

		GameMenuViews.printCommandInstructions();
		GameMenuViews.printProceedMessage();

		try {
			gameMapService.assemblyScreenMap(characters);
		} catch (GameException e) {
			System.out.println(getScriptAlert(e.getErrorKey()));
			start(userList, userPrincipal);
		}

		scanner.nextLine();

		play(gameMapService, characters, character);
	}

	/**
	 * Manage the resume game feature.
	 */
	public static void resumeGame() {

		GameMenuViews.printResumeGameMenu(userPrincipal.getGames());
		GameMenuViews.printMessage("character.choice.back");
		String option = scanner.nextLine();
		Integer chosen;
		String gameName;

		try {
			chosen = Integer.valueOf(option);
			gameName = userPrincipal.getGames().get(chosen - 1);
		} catch (Exception e) {
			return;
		}

		CharacterService characterService = new CharacterService();
		List<Character> characters = new ArrayList<>();

		try {
			characters.addAll(characterService.retrieveSavedGame(userPrincipal.getUsername(), gameName));
		} catch (GameException e) {
			System.out.println(getScriptAlert(e.getErrorKey()));
			start(userList, userPrincipal);
		}

		GameMapService gameMapService = new GameMapService();

		GameMenuViews.printCommandInstructions();
		GameMenuViews.printProceedMessage();

		Character character = new Character();

		try {
			gameMapService.assemblyScreenMap(characters);
			character = characters.stream().filter(ch -> Profile.PLAYER == ch.getProfile()).findFirst()
					.orElseThrow(() -> new GameException("error.character.list"));
		} catch (GameException e) {
			System.out.println(getScriptAlert(e.getErrorKey()));
			start(userList, userPrincipal);
		}

		scanner.nextLine();

		play(gameMapService, characters, character);
	}

	/**
	 * Manages the game play, the moves and action of the character on the map.
	 *
	 * @param gameMapService service from the map of the game.
	 * @param characters     list of characters.
	 * @param character      player.
	 */
	private static void play(final GameMapService gameMapService, final List<Character> characters, final Character character) {

		boolean playAgain = true;

		while (playAgain) {

			ScreenService.printScreen(gameMapService.getScreenMatrix());
			GameMenuViews.printMessage("game.play.option.text");
			String option = scanner.nextLine();
			int x, y;

			switch (option) {
			case "a":
				x = character.getxPosition() - 1;
				playAgain = makeMoves(gameMapService, characters, character, x, character.getyPosition());
				break;
			case "d":
				x = character.getxPosition() + 1;
				playAgain = makeMoves(gameMapService, characters, character, x, character.getyPosition());
				break;
			case "w":
				y = character.getyPosition() - 1;
				playAgain = makeMoves(gameMapService, characters, character, character.getxPosition(), y);
				break;
			case "s":
				y = character.getyPosition() + 1;
				playAgain = makeMoves(gameMapService, characters, character, character.getxPosition(), y);
				break;
			case "0":
				playAgain = false;
				break;
			case "1":
				saveGame(characters);
				break;
			case "2":
				showMapInstructions();
				break;
			case "3":
				showCommandInstructions();
				break;
			default:

			}
		}
		start(userList, userPrincipal);
	}

	/**
	 * Save the game during the play.
	 *
	 * @param characters list of characters to save.
	 */
	public static void saveGame(final List<Character> characters) {

		GameMenuViews.printMessage("game.play.save");
		String gameName = scanner.nextLine();

		userPrincipal.getGames().add(gameName);

		try {

			UserService userService = new UserService();
			userService.saveAll(userList);

			CharacterService characterService = new CharacterService();
			characterService.saveAll(characters, userPrincipal.getUsername(), gameName);

		} catch (GameException e) {
			System.out.println(getScriptAlert(e.getErrorKey()));
		}
	}

	/**
	 * Evaluate the next step and make the moves if possible.
	 *
	 * @param gameMapService game map to be updated.
	 * @param characters     list of characters for fight action.
	 * @param character      player
	 * @param x              position x of the next move.
	 * @param y              position y of the next move.
	 * @return boolean to break the while loop.
	 */
	private static boolean makeMoves(final GameMapService gameMapService, final List<Character> characters,
									 final Character character, final int x, final int y) {

		String symbol = gameMapService.retrieveSymbolFromMap(x, y);

		if (symbol.contains(Symbol.SEA.build()) || symbol.contains(Symbol.BORDER.build())) {
			return true;
		}

		gameMapService.setPreviousSymbolOnMap(character.getxPosition(), character.getyPosition(), character);
		character.setLastLandPosition(gameMapService.retrieveSymbolFromMap(character.getxPosition(), character.getyPosition()));

		String keepPreviousSymbol = "";

		if (symbol.contains(Symbol.ALLY.build()) || symbol.contains(Symbol.ENEMY.build())) {
			fightStart(gameMapService, characters, character, x, y);
			keepPreviousSymbol = gameMapService.getPreviousSimbol();
		}

		character.setxPosition(x);
		character.setyPosition(y);
		gameMapService.setCharacterOnMapLocation(x, y);
		if (!"".equals(keepPreviousSymbol)) gameMapService.setPreviousSimbol(keepPreviousSymbol);

		return character.isStatus();
	}

	/**
	 * Starts a fight between two characters.
	 *
	 * @param gameMapService map service of the game.
	 * @param characters     list of characters.
	 * @param player         character chosed by the user.
	 * @param x              x position.
	 * @param y              y position.
	 */
	private static void fightStart(final GameMapService gameMapService, final List<Character> characters,
								   final Character player, final int x, final int y) {

		try {
			Character opponent = characters.stream().filter(ch -> x == ch.getxPosition() && y == ch.getyPosition())
					.findFirst().orElseThrow(() -> new GameException(""));

			int initialFightLife = player.getLife();

			boolean again = true;

			while (again) {

				CharachterView.printCharactersFightInfo(opponent, player);

				if (Profile.ALLY == opponent.getProfile()) {
					GameMenuViews.printAllyFightMenu();
				} else {
					GameMenuViews.printEnemyFightMenu();
				}

				String option = scanner.nextLine();

				switch (option) {
				case "1":
					again = fight(opponent, player, initialFightLife, gameMapService);
					break;
				case "2":
					again = decideAlliancePosition(opponent, player, gameMapService);
					break;
				default:
					gameMapService.setPreviousSimbol(Symbol.getSymbol(opponent.getProfile().build()).build());
					again = false;
				}
			}

		} catch (GameException e) {
			System.out.println(getScriptAlert(e.getErrorKey()));
		}
	}

	/**
	 * Responsible for the logic of the attack, reducing life from each other.
	 *
	 * @param opponent         application character competitor with the player.
	 * @param player           the user character.
	 * @param initialFightLife the life of the player before the fight.
	 * @param gameMapService   map service to update positions
	 * @return boolean
	 */
	private static boolean fight(final Character opponent, final Character player, final int initialFightLife,
								 final GameMapService gameMapService) {

		Integer playerAttack = getRandomRange(player.getDamage());
		Integer oponentAttack = getRandomRange(opponent.getDamage());
		Integer playerArmour = getRandomRange(player.getArmour());
		Integer oponentArmour = getRandomRange(opponent.getArmour());

		player.setLife(player.getLife() - oponentAttack + (playerArmour > oponentAttack ? 0 : playerArmour));
		opponent.setLife(opponent.getLife() - playerAttack + (oponentArmour > playerAttack ? 0 : oponentArmour));

		if (opponent.getLife() <= 0) {
			fightCollect(player, opponent, initialFightLife);
			setCharacterDefeatAttributes(opponent);
			gameMapService.setPreviousSimbol(Symbol.DEFEATED.build());
			GameMenuViews.printBigMessage("game.win");
			scanner.nextLine();
			return false;
		}

		if (player.getLife() <= 0) {
			GameMenuViews.printBigMessage("game.loose");
			scanner.nextLine();
			start(userList, userPrincipal);
			return false;
		}

		return true;
	}

	/**
	 * Responsible for the logic to use allience.
	 *
	 * @param opponent       application character competitor with the player.
	 * @param player         the user character.
	 * @param gameMapService map service to update positions
	 * @return boolean
	 */
	private static boolean decideAlliancePosition(final Character opponent, final Character player,
												  final GameMapService gameMapService) {

		if (!opponent.getEnemies().contains(player.getName())
				&& (opponent.getAllies().contains(player.getName()) || getRandomRange(Arrays.asList(1, 100)) % 2 == 0)) {

			allyCollect(opponent, player);
			setCharacterDefeatAttributes(opponent);
			gameMapService.setPreviousSimbol(Symbol.DEFEATED.build());
			return false;

		} else {
			GameMenuViews.printMessage("game.play.fight.action.ally.denied");
			return true;
		}
	}

	/**
	 * Sets the character defated attributes.
	 *
	 * @param character opponent character that looses the battle.
	 */
	private static void setCharacterDefeatAttributes(final Character character) {
		character.setLife(0);
		character.setStatus(false);
		character.setProfile(Profile.DEFEATED);
	}

	/**
	 * Calculate a randomic value for the range in the list
	 *
	 * @param range range of two values
	 * @return the randomic number
	 */
	private static Integer getRandomRange(final List<Integer> range) {
		return new Random().nextInt(range.get(1)) + range.get(0);
	}

	/**
	 * Give the prizes of the victory to the player.
	 *
	 * @param player      user character.
	 * @param opponent    application character competitor with the player.
	 * @param initialLife the life of the player before the fight.
	 */
	private static void fightCollect(final Character player, final Character opponent, final int initialLife) {

		player.setLife(initialLife + opponent.getExperienceShared().multiply(new BigDecimal(opponent.getLife())).intValue());

		player.setDamage(player.getDamage().stream().map(damage -> damage + opponent.getExperienceShared()
				.multiply(new BigDecimal(getRandomRange(opponent.getDamage()))).intValue()).collect(Collectors.toList()));

		player.setArmour(player.getArmour().stream().map(armour -> armour + opponent.getExperienceShared()
				.multiply(new BigDecimal(getRandomRange(opponent.getArmour()))).intValue()).collect(Collectors.toList()));
	}

	/**
	 * Gives the help from the ally to the player.
	 * @param player user characters.
	 * @param opponent application character competitor with the player.
	 */
	private static void allyCollect(final Character player, final Character opponent) {
		player.setLife(
				opponent.getExperienceShared().multiply(new BigDecimal(opponent.getLife())).intValue() / ALLY_SHARED_EXPERIENCE_DIVISOR);

		player.setDamage(player.getDamage().stream().map(damage -> damage + opponent.getExperienceShared()
				.multiply(new BigDecimal(getRandomRange(opponent.getDamage()))).intValue() / ALLY_SHARED_EXPERIENCE_DIVISOR)
				.collect(Collectors.toList()));

		player.setArmour(player.getArmour().stream().map(armour -> armour + opponent.getExperienceShared()
				.multiply(new BigDecimal(getRandomRange(opponent.getArmour()))).intValue() / ALLY_SHARED_EXPERIENCE_DIVISOR)
				.collect(Collectors.toList()));
	}

	/**
	 * Manage the game exploration.
	 */
	private static void explore() {

		GameMenuViews.printExploreGameMenu();
		String option = scanner.nextLine();

		switch (option) {
		case "1":
			exploreCharacters();
			break;
		case "2":
			// Explore Map
			break;
		case "3":
			gameInstructions();
			break;
		default:
			start(userList, userPrincipal);
		}
	}

	/**
	 * Manage the characters explore.
	 */
	private static void exploreCharacters() {

		CharacterController characterController = new CharacterController();

		Character character = characterController.charactersChoice();

		if (null == character) explore();

		characterController.printCharacter(character);

		explore();
	}

	/**
	 * Manage the game instructions from explore menu.
	 */
	private static void gameInstructions() {

		GameMenuViews.printGameInstructionsMenu();
		String option = scanner.nextLine();

		switch (option) {
		case "1":
			showMapInstructions();
			gameInstructions();
			break;
		case "2":
			showCommandInstructions();
			gameInstructions();
			break;
		}
		explore();
	}

	/**
	 * Prints the map instruction in the terminal and wait any action.
	 */
	private static void showMapInstructions() {
		GameMenuViews.printMapInstructions();
		GameMenuViews.printMessage("game.menu.goback");
		scanner.nextLine();
	}

	/**
	 * Prints the commmand instruction in the terminal and wait any action.
	 */
	private static void showCommandInstructions() {
		GameMenuViews.printCommandInstructions();
		GameMenuViews.printMessage("game.menu.goback");
		scanner.nextLine();
	}

	/**
	 * Manage the game exit.
	 */
	private static void exit() {
		System.out.println();
		System.out.println(getBoldScriptText("game.exit.message"));
		System.exit(-1);
	}
}
