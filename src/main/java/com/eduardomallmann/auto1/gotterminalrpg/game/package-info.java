/**
 * Provides the necessary objects to manage the game.
 *
 * @since 1.0
 */
package com.eduardomallmann.auto1.gotterminalrpg.game;