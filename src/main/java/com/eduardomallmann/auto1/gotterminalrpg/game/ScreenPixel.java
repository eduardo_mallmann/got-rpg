package com.eduardomallmann.auto1.gotterminalrpg.game;

import java.util.Objects;

/**
 * Responsible to kept the smallest piece of image of the map.
 */
public class ScreenPixel {

	private int x, y;
	private String symbol;

	/**
	 * Main constructor.
	 */
	public ScreenPixel() {
	}

	/**
	 * Full constructor.
	 *
	 * @param x      x position in the map.
	 * @param y      y position in the map.
	 * @param symbol type of the piece.
	 */
	public ScreenPixel(int x, int y, String symbol) {
		this.x = x;
		this.y = y;
		this.symbol = symbol;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ScreenPixel that = (ScreenPixel) o;
		return x == that.x &&
				y == that.y &&
				Objects.equals(symbol, that.symbol);
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y, symbol);
	}

	@Override
	public String toString() {
		return "ScreenPixel{" +
				"x=" + x +
				", y=" + y +
				", build='" + symbol + '\'' +
				'}';
	}
}
