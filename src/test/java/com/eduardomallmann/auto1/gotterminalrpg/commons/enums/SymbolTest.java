package com.eduardomallmann.auto1.gotterminalrpg.commons.enums;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SymbolTest {

    private String resultHolder;

    @Before
    public void setup() {
        resultHolder = "";
    }

    @Test
    public void build_ShouldRetrunSymbolValue() {
        //when
        resultHolder = Symbol.BORDER.build();
        //then
        assertEquals("#", resultHolder);
    }

    @Test
    public void isPlayer_ShouldReturnBoolean() {
        //given
        String player = "\u001B[90mi\u001B[0m";
        //when
        boolean resultHolder = Symbol.isPlayer(player);
        //then
        assertTrue(resultHolder);
    }

    @Test
    public void isAly_ShouldReturnBoolean() {
        //given
        String aly = "\u001B[34mi\u001B[0m";
        //when
        boolean resultHolder = Symbol.isAlly(aly);
        //then
        assertTrue(resultHolder);
    }

    @Test
    public void isEnemy_ShouldReturnBoolean() {
        //given
        String enemy = "\u001B[31mi\u001B[0m";
        //when
        boolean resultHolder = Symbol.isEnemy(enemy);
        //then
        assertTrue(resultHolder);
    }

    @Test
    public void isDefeated_ShouldReturnBoolean() {
        //given
        String defeated = "\u001B[31mx\u001B[0m";
        //when
        boolean resultHolder = Symbol.isDefeated(defeated);
        //then
        assertTrue(resultHolder);
    }

    @Test
    public void isSea_ShouldReturnBoolean() {
        //given
        String sea = "\u2592";
        //when
        boolean resultHolder = Symbol.isSea(sea);
        //then
        assertTrue(resultHolder);
    }

    @Test
    public void isBorder_ShouldReturnBoolean() {
        //given
        String border = "#";
        //when
        boolean resultHolder = Symbol.isBorder(border);
        //then
        assertTrue(resultHolder);
    }

    @Test
    public void isLand_ShouldReturnBoolean() {
        //given
        String land = "\u2593";
        //when
        boolean resultHolder = Symbol.isLand(land);
        //then
        assertTrue(resultHolder);
    }

    @Test
    public void isWarrior_ShouldReturnBoolean() {
        //given
        String enemy = "\u001B[31mi\u001B[0m";
        //when
        boolean resultHolder = Symbol.isWarrior(enemy);
        //then
        assertTrue(resultHolder);
    }

    @Test
    public void isLandscape_ShouldReturnBoolean() {
        //given
        String border = "#";
        //when
        boolean resultHolder = Symbol.isLandscape(border);
        //then
        assertTrue(resultHolder);
    }

    @Test
    public void getSymbol_ShouldReturnSymbol() {
        //given
        String border = "#";
        //when
        Symbol resultHolder = Symbol.getSymbol(border);
        //then
        assertEquals(border, resultHolder.build());
    }


}
