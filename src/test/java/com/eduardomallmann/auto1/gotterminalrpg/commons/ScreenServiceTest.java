package com.eduardomallmann.auto1.gotterminalrpg.commons;

import com.eduardomallmann.auto1.gotterminalrpg.characters.Character;
import com.eduardomallmann.auto1.gotterminalrpg.characters.CharacterService;
import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;
import com.eduardomallmann.auto1.gotterminalrpg.game.GameMapService;
import org.junit.Before;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ScreenServiceTest {

	@Test
	public void drawnTitle_ShouldReturnBufferedImage() {
		//given
		int width = 30, height = 20;
		//when
		BufferedImage image = ScreenService.drawnTitle(width,height,"test", 0,0);
		//then
		assertEquals(width, image.getWidth());
		assertEquals(height, image.getHeight());
	}

	@Test
	public void printScreen_ShouldPrintBufferedImage() {
		//given
		BufferedImage image = ScreenService.drawnTitle(30,20,"test", 0,0);
		//when
		ScreenService.printScreen(image);
		//then
		assertTrue(true);
	}

	@Test
	public void printScreen_ShouldPrintScreenMatrixStandardSize() throws GameException {
		//given
		CharacterService characterService = new CharacterService();
		List<Character> characters = characterService.retrieveAll();
		List<Character> characterList = characterService.buildCharactersGameProfile(characters.get(0));
		GameMapService gameMapService = new GameMapService();
		//when
		gameMapService.assemblyScreenMap(characterList);
		//and
		ScreenService.printScreen(gameMapService.getScreenMatrix());
		//then
		assertTrue(true);
	}

	@Test
	public void printScreen_ShouldPrintScreenMatrixInformedSize() throws GameException {
		//given
		CharacterService characterService = new CharacterService();
		List<Character> characters = characterService.retrieveAll();
		List<Character> characterList = characterService.buildCharactersGameProfile(characters.get(0));
		GameMapService gameMapService = new GameMapService();
		//when
		gameMapService.assemblyScreenMap(characterList);
		ScreenService.printScreen(gameMapService.getScreenMatrix(), 36, 43);
		//then
		assertTrue(true);
	}

}
