package com.eduardomallmann.auto1.gotterminalrpg.commons.enums;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class LandsTest {

	@Test
	public void retrieveByName_ShouldReturnAnInstance() {
		//given
		String name = "Lannister";
		//when
		Lands lands = Lands.retrieveByName(name);
		//then
		assertTrue(lands.name().equalsIgnoreCase(name));
	}

	@Test
	public void retrieveByLand_ShouldReturnAnInstance() {
		//given
		String land = "\u001B[102m\u001B[92m\u2593\u001B[0m";
		String value = "Tyrell";
		//when
		Lands lands = Lands.retrieveByLand(land);
		//then
		assertTrue(lands.name().equalsIgnoreCase(value));
	}

}
