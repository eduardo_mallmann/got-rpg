package com.eduardomallmann.auto1.gotterminalrpg.commons.utils;

import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.TerminalColor;
import org.junit.Before;
import org.junit.Test;

import java.util.Objects;

import static com.eduardomallmann.auto1.gotterminalrpg.commons.utils.ResourceUtils.*;
import static org.junit.Assert.assertTrue;

public class ResourceUtilsTest {

    private String resultHolder;

    @Before
    public void setup() {
        resultHolder = "";
    }

    @Test
    public void getScript_ShouldReturnAString() {
        //when
        resultHolder = getScript("script.test");
        //then
        assertTrue(resultHolder.length() > 0);
    }

    @Test
    public void getBoldScriptText_ShouldReturnAStringWithBoldCharacter() {
        //when
        resultHolder = getBoldScriptText("script.test");
        //then
        assertTrue(resultHolder.contains("[1;"));
    }

    @Test
    public void getScriptAlert_ShouldReturnAString() {
        //when
        resultHolder = getScriptAlert("script.test");
        //then
        assertTrue(resultHolder.contains(TerminalColor.RED.bold()));
        assertTrue(resultHolder.contains(TerminalColor.BK_BRIGHT_YELLOW.build()));
    }

    @Test
    public void getConfig_ShouldReturnAString() {
        //when
        resultHolder = getConfig("script.test");
        //then
        assertTrue(resultHolder.length() > 0);
    }

    @Test
    public void getObjectValue_ShouldReturnAnObject() {
        //given
        Object object = null;
        ResourceUtils ru = new ResourceUtils("test");
        //when
        object = ru.getObjectValue("test");
        //then
        assertTrue(Objects.nonNull(object));
    }

    @Test
    public void getStringValue_ShouldReturnAString() {
        //given
        ResourceUtils ru = new ResourceUtils("test");
        //when
        resultHolder = ru.getStringValue("test");
        //then
        assertTrue(resultHolder.length() > 0);
    }

}
