package com.eduardomallmann.auto1.gotterminalrpg.commons.utils;

import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FileUtilsTest {

    private String fileName;
    private List<String> content;

    @Before
    public void setup() {
        fileName = "unit_testing.csv";
        content = new ArrayList<>();
    }

    @After
    public void clean() {

        Path folderPath = Paths.get(FileUtils.SYSTEM_FOLDER_PATH);

        if (Files.exists(folderPath)) {
            try {

                List<Path> files = Files.list(folderPath).collect(Collectors.toList());

                if (!files.isEmpty()) {
                    for (Path file : files) {
                        Files.delete(file);
                    }
                }

                Files.delete(folderPath);

            } catch (IOException io) {
                System.out.println("Cleaning FileUtilsTest not working");
            }
        }
    }

    @Test
    public void createAppFolder_ShouldReturnPath() throws GameException {

        //given
        Path appFolder;
        //when
        appFolder = FileUtils.createAppFolder();
        //then
        assertEquals(FileUtils.SYSTEM_FOLDER_PATH, appFolder.toString());

    }

    @Test
    public void saveFile_ShouldCreateNewFile() throws GameException {

        //when
        FileUtils.saveFile(fileName, content);
        //then
        assertTrue(Files.exists(Paths.get(FileUtils.SYSTEM_FOLDER_PATH.concat(File.separator).concat(fileName))));
    }

    @Test
    public void saveFile_ShouldHaveContent() throws IOException, GameException {
        //given
        content.add("Test1");
        content.add("Test2");
        List<String> fileRead;
        //when
        FileUtils.saveFile(fileName, content);
        //and
        fileRead = Files.lines(Paths.get(FileUtils.SYSTEM_FOLDER_PATH.concat(File.separator).concat(fileName)))
                .collect(Collectors.toList());
        //then
        assertTrue(fileRead.containsAll(content));
    }



}
