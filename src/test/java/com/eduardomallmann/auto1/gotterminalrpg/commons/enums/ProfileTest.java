package com.eduardomallmann.auto1.gotterminalrpg.commons.enums;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProfileTest {

	@Test
	public void getProfile_ShouldReturnAnObjectOfProfile() {
		//given
		String profile = "player";
		//when
		Profile responseHolder = Profile.getProfile(profile);
		//then
		assertEquals(profile, responseHolder.build());
	}

}
