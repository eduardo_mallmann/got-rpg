package com.eduardomallmann.auto1.gotterminalrpg.commons.enums;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TerminalColorTest {

    private String resultHolder;

    @Before
    public void setup() {
        resultHolder = "";
    }

    @Test
    public void build_ShouldReturnValue() {
        //when
        resultHolder = TerminalColor.BK_YELLOW.build();
        //then
        assertEquals((char) 27 + "[" + "43m", resultHolder);
    }

    @Test
    public void bold_ShouldReturnBoldValue() {
        //when
        resultHolder = TerminalColor.YELLOW.bold();
        //then
        assertEquals((char) 27 + "[1;" + "33m", resultHolder);
    }

    @Test
    public void bold_ShouldNotReturnBoldValue() {
        //when
        resultHolder = TerminalColor.BK_YELLOW.bold();
        //then
        assertEquals((char) 27 + "[" + "43m", resultHolder);
    }

    @Test
    public void bold_ShouldReturnUnderlineValue() {
        //when
        resultHolder = TerminalColor.YELLOW.underline();
        //then
        assertEquals((char) 27 + "[4;" + "33m", resultHolder);
    }

    @Test
    public void bold_ShouldNotReturnUnderlineValue() {
        //when
        resultHolder = TerminalColor.BK_YELLOW.underline();
        //then
        assertEquals((char) 27 + "[" + "43m", resultHolder);
    }

    @Test
    public void getBold_ShouldReturnBoldCharacter() {
        //when
        resultHolder = TerminalColor.getBold();
        //then
        assertEquals((char) 27 + "[1m", resultHolder);
    }

    @Test
    public void getBold_ShouldReturnUnderlineCharacter() {
        //when
        resultHolder = TerminalColor.getUnderline();
        //then
        assertEquals((char) 27 + "[4m", resultHolder);
    }

    @Test
    public void reset_ShouldReturnResetCharacter() {
        //when
        resultHolder = TerminalColor.RESET();
        //then
        assertEquals((char) 27 + "[0m", resultHolder);
    }

    @Test
    public void getColor_ShouldReturnTerminalColor() {
        //given
        String color = "black";
        //when
        TerminalColor terminalColor = TerminalColor.getColor(color);
        //then
        assertTrue(color.equalsIgnoreCase(terminalColor.name()));
    }

    @Test
    public void getColor_ShouldReturnBrightTerminalColor() {
        //given
        String color = "black";
        //when
        TerminalColor terminalColor = TerminalColor.getBrightColor(color);
        //then
        assertEquals((char) 27 + "[90m", terminalColor.build());
    }

    @Test
    public void getColor_ShouldReturnBackgroundTerminalColor() {
        //given
        String color = "black";
        //when
        TerminalColor terminalColor = TerminalColor.getBKColor(color);
        //then
        assertEquals((char) 27 + "[40m", terminalColor.build());
    }

    @Test
    public void getColor_ShouldReturnBrightBackgroundTerminalColor() {
        //given
        String color = "black";
        //when
        TerminalColor terminalColor = TerminalColor.getBKBrightColor(color);
        //then
        assertEquals((char) 27 + "[100m", terminalColor.build());
    }


}
