package com.eduardomallmann.auto1.gotterminalrpg.characters;

import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.eduardomallmann.auto1.gotterminalrpg.characters.CharachterView.printCharactersFightInfo;
import static org.junit.Assert.assertTrue;

public class CharacterViewTest {

	private CharachterView charachterView;
	private List<Character> characters;

	@Before
	public void setup() throws GameException {
		charachterView = new CharachterView();
		CharacterService characterService = new CharacterService();
		characters = characterService.retrieveAll();
	}

	@Test
	public void printCharacterListMenu_ShouldPrintCharactersList() {
		//when
		charachterView.printCharacterListMenu(characters);
		//then
		assertTrue(true);
	}

	@Test
	public void printCharacterInfo_ShouldPrintCharacterInfo() throws GameException {
		//given
		Character character = characters.get(1);
		//when
		charachterView.printCharacterInfo(character);
		//then
		assertTrue(true);
	}

	@Test
	public void printCharactersFightInfo_ShouldPrintCharactersInfo() throws GameException {
		//given
		Character character = characters.get(1);
		Character otherCharacter = characters.get(4);
		//when
		printCharactersFightInfo(character, otherCharacter);
		//then
		assertTrue(true);
	}

}
