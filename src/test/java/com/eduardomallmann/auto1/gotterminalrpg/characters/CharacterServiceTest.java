package com.eduardomallmann.auto1.gotterminalrpg.characters;

import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.Profile;
import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;
import com.eduardomallmann.auto1.gotterminalrpg.commons.utils.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CharacterServiceTest {

	private static final Path FILE_PATH = Paths.get(FileUtils.SYSTEM_FOLDER_PATH.concat(File.separator)
			.concat("test_test_".concat(CharacterService.CHARACTER_FILE_NAME)));

	private String wrappedCharacter;
	private Character character;
	private Character otherCharacter;
	private Character anotherCharacter;
	private CharacterService characterService;
	private List<Character> characterList = new ArrayList<>();

	@Before
	public void setup() {

		characterService = new CharacterService();

		wrappedCharacter = "1, John Snow, Stark, initial, Aria Stark; Sansa Stark, Cersei Lannister; Jamie Lannister, true, 0, 0, 100, "
				+ "10-15, 8-12, 0.40, ";

		character = Character.builder()
				.id("1")
				.name("John Snow")
				.house("Stark")
				.profile("initial")
				.allies(Arrays.asList("Aria Stark", "Sansa Stark"))
				.enemies(Arrays.asList("Cersei Lannister", "Jamie Lannister"))
				.status(true)
				.xPosition(0)
				.yPosition(0)
				.life(100)
				.damage("10-15")
				.armour("8-12")
				.experienceShared("0.40")
				.lastLandPosition(null)
				.build();
		anotherCharacter = Character.builder()
				.id("2")
				.name("Aria Stark")
				.house("Stark")
				.profile("initial")
				.allies(Arrays.asList("John Snow", "Sansa Stark"))
				.enemies(Arrays.asList("Cersei Lannister", "Jamie Lannister"))
				.status(true)
				.xPosition(0)
				.yPosition(0)
				.life(110)
				.damage("13-18")
				.armour("8-12")
				.experienceShared("0.40")
				.lastLandPosition(null)
				.build();
		otherCharacter = Character.builder()
				.id("3")
				.name("Jamie Lannister")
				.house("Lannister")
				.profile(Profile.INITIAL)
				.allies(Arrays.asList("Cersei Lannister", "Tyrion Lannister"))
				.enemies(Arrays.asList("John Snow", "Aria Stark"))
				.status(true)
				.xPosition(0)
				.yPosition(0)
				.life(95)
				.damage("12-13")
				.armour("8-15")
				.experienceShared("0.50")
				.lastLandPosition(null)
				.build();

		characterList.add(this.character);
		characterList.add(this.otherCharacter);
		characterList.add(this.anotherCharacter);
	}

	@After
	public void clean() {
		if (Files.exists(FILE_PATH)) {
			try {
				Files.delete(FILE_PATH);
			} catch (IOException io) {
				System.out.println("Cleaning CharacterServiceTest not working");
			}
		}
	}

	@Test
	public void retrieveAll_ShouldReturnAListOfCharactersFromCSV() throws GameException {
		//when
		List<Character> responseHolder = characterService.retrieveAll();
		//then
		assertEquals(25, responseHolder.size());
	}

	@Test
	public void saveAll_ShouldSaveUsersListInCSVFile() throws GameException {
		//when
		characterService.saveAll(characterList, "test", "test");
		//and
		List<Character> responseHolder = this.retriveCharacters();
		//then
		assertTrue(responseHolder.containsAll(characterList));
	}

	@Test
	public void retrieveSavedGame_ShouldReturnAListOfCharactersFromFile() throws GameException {
		//given
		characterService.saveAll(characterList, "test", "test");
		//when
		List<Character> responseHolder = new ArrayList<>(characterService.retrieveSavedGame("test", "test"));
		//then
		assertTrue(responseHolder.containsAll(characterList));
	}

	@Test
	public void buildCharactersGameProfile_ShouldSetCharactersProfiles() throws GameException {
		//when
		List<Character> characters = characterService.buildCharactersGameProfile(character);
		//then
		assertTrue(characters.stream().allMatch(ch -> Profile.INITIAL != ch.getProfile()));
	}

	@Test
	public void wrapCSV_ShouldReturnAnUserWrappedInCSVFormat() {
		//when
		String resultHolder = characterService.wrapCSV(character);
		//then
		assertEquals(wrappedCharacter, resultHolder);
	}

	@Test
	public void unwrapCSV_ShouldReturnAnUser() {
		//when
		Character resultHolder = characterService.unwrapCSV(wrappedCharacter);
		//then
		assertEquals(character, resultHolder);
	}

	private List<Character> retriveCharacters() throws GameException {

		try (Stream<String> stream = Files.lines(FILE_PATH)) {

			return stream.parallel().map(characterService::unwrapCSV).collect(Collectors.toList());

		} catch (IOException e) {
			throw new GameException("error.character.list");
		}
	}
}
