package com.eduardomallmann.auto1.gotterminalrpg.users;

import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class LoginViewTest {

    private LoginView view;
    private List<User> userList = new ArrayList<>();

    @Before
    public void setup() {

        view = new LoginView();

        User user = new User("test", "test");
        user.getGames().add("game1");
        user.getGames().add("game2");
        User otherUser = new User("test1", "test1");

        userList.add(user);
        userList.add(otherUser);
    }


    @Test
    public void printLoginScreen_ShouldPrintLoginScreenInTerminal() {
        //when
        view.printLoginScreen();
        //then
        assertTrue(true);
    }

    @Test
    public void printAlertUserName_ShouldPrintUserNameAlertInTerminal() {
        //when
        view.printAlertUserName();
        //then
        assertTrue(true);
    }

    @Test
    public void printPassword_ShouldPrintPasswordRequestInTerminal() {
        //when
        view.printPassword();
        //then
        assertTrue(true);
    }

    @Test
    public void printAlertPassword_ShouldPrintPasswordAlertInTerminal() {
        //when
        view.printAlertPassword();
        //then
        assertTrue(true);
    }

    @Test
    public void printSaveExceptionMessage_ShouldPrintSaveExceptionMessageInTerminal() {
        //given
        GameException e = new GameException("error.files.save");
        //when
        view.printSaveExceptionMessage(e);
        //then
        assertTrue(true);
    }

}
