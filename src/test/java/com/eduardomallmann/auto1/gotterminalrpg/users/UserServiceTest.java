package com.eduardomallmann.auto1.gotterminalrpg.users;

import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;
import com.eduardomallmann.auto1.gotterminalrpg.commons.utils.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserServiceTest {

    private String wrappedUser;
    private User user;
    private UserService userService;
    private List<User> userList = new ArrayList<>();

    @Before
    public void setup() {

        userService = new UserService();

        user = new User("test", "test");
        user.getGames().add("game1");
        user.getGames().add("game2");
        User otherUser = new User("test1", "test1");

        userList.add(user);
        userList.add(otherUser);

        wrappedUser = "test, test, game1; game2";
    }

    @After
    public void clean() {
        Path filePath = Paths.get(FileUtils.SYSTEM_FOLDER_PATH.concat(File.separator).concat(UserService.USER_FILE_NAME));

        if (Files.exists(filePath)) {
            try {

                Files.delete(filePath);

            } catch (IOException io) {
                System.out.println("Cleaning UserServiceTest not working");
            }
        }
    }

    @Test
    public void retrieveAll_ShouldReturnAListOfUsersFromCSV() throws GameException {
        //when
        userService.saveAll(userList);
        //and
        List<User> responseHolder = userService.retrieveAll();
        //then
        assertTrue(responseHolder.containsAll(userList));
    }

    @Test
    public void saveAll_ShouldSaveUsersListInCSVFile() throws GameException {
        //when
        userService.saveAll(userList);
        //and
        List<User> responseHolder = userService.retrieveAll();
        //then
        assertTrue(responseHolder.containsAll(userList));
    }

    @Test
    public void wrapCSV_ShouldReturnAnUserWrappedInCSVFormat() {
        //when
        String resultHolder = userService.wrapCSV(user);
        //then
        assertEquals(wrappedUser, resultHolder);
    }

    @Test
    public void unwrapCSV_ShouldReturnAnUser() {
        //when
        User resultHolder = userService.unwrapCSV(wrappedUser);
        //then
        assertEquals(user, resultHolder);
    }
}
