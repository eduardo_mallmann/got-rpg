package com.eduardomallmann.auto1.gotterminalrpg.game;

import com.eduardomallmann.auto1.gotterminalrpg.characters.Character;
import com.eduardomallmann.auto1.gotterminalrpg.characters.CharacterService;
import com.eduardomallmann.auto1.gotterminalrpg.commons.enums.Symbol;
import com.eduardomallmann.auto1.gotterminalrpg.commons.exceptions.GameException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class GameMapServiceTest {

	@Test
	public void assemblyScreenMap_ShouldAssemblyTheMap() throws GameException {
		//given
		CharacterService characterService = new CharacterService();
		Character character = characterService.retrieveAll().get(0);
		List<Character> characters = characterService.buildCharactersGameProfile(character);
		GameMapService gameMapService = new GameMapService();
		//when
		gameMapService.assemblyScreenMap(characters);
		//then
		Assert.assertTrue(Arrays.stream(gameMapService.getScreenMatrix()).allMatch(Objects::nonNull));
	}

	@Test
	public void setCharacterOnMapLocation_ShouldInferedDataOnScreen() {
		//given
		int x = 10, y = 20;
		GameMapService gameMapService = new GameMapService();
		//when
		gameMapService.setCharacterOnMapLocation(x,y);
		//then
		Assert.assertEquals(Symbol.PLAYER.build(), gameMapService.getScreenMatrix()[y][x]);
	}

	@Test
	public void retrieveSymbolFromMap_ShouldReturnAString() {
		//given
		int x = 10, y = 20;
		GameMapService gameMapService = new GameMapService();
		gameMapService.setCharacterOnMapLocation(x,y);
		//when
		String responseHolder = gameMapService.retrieveSymbolFromMap(x,y);
		//then
		Assert.assertEquals(Symbol.PLAYER.build(), responseHolder);
	}

}
