package com.eduardomallmann.auto1.gotterminalrpg.game;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class GameMenuViewsTest {

	@Test
	public void printInitialMenu_ShouldPrintInitialMenuInTerminal() {
		//when
		GameMenuViews.printInitialMenu();
		//then
		assertTrue(true);
	}

	@Test
	public void printProceedMessage_ShouldPrintProceedMessageInTerminal() {
		//when
		GameMenuViews.printProceedMessage();
		//then
		assertTrue(true);
	}

	@Test
	public void printGoBackMessage_ShouldPrintGoBackMessageInTerminal() {
		//when
		GameMenuViews.printMessage("game.menu.goback");
		//then
		assertTrue(true);
	}

	@Test
	public void printResumeGameMenu_ShouldPrintResumeGameInTerminal() {
		//given
		List<String> gamesSaved = Arrays.asList("Game 1", "Game 2", "Game 3");
		//when
		GameMenuViews.printResumeGameMenu(gamesSaved);
		//then
		assertTrue(true);
	}

	@Test
	public void printExploreGameMenu_ShouldPrintExploreGameMenuInTerminal() {
		//when
		GameMenuViews.printExploreGameMenu();
		//then
		assertTrue(true);
	}

	@Test
	public void printGameInstructionsMenu_ShouldPrintGameInstructionsMenuInTerminal() {
		//when
		GameMenuViews.printGameInstructionsMenu();
		//then
		assertTrue(true);
	}

	@Test
	public void printMapInstructions_ShouldPrintMapInstructionsInTerminal() {
		//when
		GameMenuViews.printMapInstructions();
		//then
		assertTrue(true);
	}

	@Test
	public void printCommandInstructions_ShouldPrintCommandInstructionsInTerminal() {
		//when
		GameMenuViews.printCommandInstructions();
		//then
		assertTrue(true);
	}

	@Test
	public void printBigMessage_ShouldPrintABigMessageInTerminal() {
		//when
		GameMenuViews.printBigMessage("game.win");
		//then
		assertTrue(true);
	}


}
